﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.UsuariodoSistema.Classes
{
    class UsuarioDTO
    {
        public int ID_User{ get; set; }
        public string Nome { get; set; }
        public string Nm_Usuario { get; set; }
        public string Perfil { get; set; }
        public string Senha { get; set; }
        public string Confirmar_Senha { get; set; }
        public string Observacao { get; set; }
        public bool Acesso_Autorizado { get; set; }
    }
}
