﻿using MySql.Data.MySqlClient;
using SIGMA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.UsuariodoSistema.Classes
{
    class DatabaseUsuario
    {
        public int SalvarUsuario(UsuarioDTO dto)
        {
            string script = @"INSERT INTO tb_usuario(nm_nome, ds_username, ds_observacao, ds_password, ds_acesso_autorizado) VALUES (@nm_nome, @ds_username, @ds_observacao, @ds_password, @ds_acesso_autorizado)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_username", dto.Nm_Usuario));
            parms.Add(new MySqlParameter("ds_observacao", dto.Observacao));
            parms.Add(new MySqlParameter("ds_password", dto.Senha));
            parms.Add(new MySqlParameter("ds_acesso_autorizado", dto.Acesso_Autorizado));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script , parms);

            return id;
        }
        public UsuarioDTO Logar(UsuarioDTO dto)
        {
            string script = @"Select * from tb_usuario
                               Where ds_username = @ds_username 
                                 and ds_password = @ds_password";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_username", dto.Nm_Usuario));
            parms.Add(new MySqlParameter("ds_password", dto.Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                dto.ID_User = reader.GetInt32("id_usuario");
                dto.Nome = reader.GetString("nm_nome");
                dto.Nm_Usuario = reader.GetString("ds_username");
                dto.Observacao = reader.GetString("ds_observacao");
                dto.Acesso_Autorizado = reader.GetBoolean("ds_acesso_autorizado");
            }
            
            return dto;
        }
    }
}
