﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.UsuariodoSistema.Classes.Perfil
{
    class BusinessPerfil
    {
        public int CriarPerfil(DTOPerfil dto)
        {
            DatabasePerfil db = new DatabasePerfil();
            int id = db.CriarPerfil(dto);
            return id;
        }
        public DTOPerfil Consultar_Perfil()
        {
            DatabasePerfil db = new DatabasePerfil();
            DTOPerfil dto = db.Consultar_Perfis();
            return dto;
        }
    }
}
