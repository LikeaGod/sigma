﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.UsuariodoSistema.Classes
{
    class DTOPerfil
    {
        public int id_acessos { get; set; }

        public string ds_nivel { get; set; }

        public string ds_perfil { get; set; }

        public Boolean perm_cliente_menu { get; set; }

        public Boolean perm_cliente_alter_dados { get; set; }

        public Boolean perm_cliente_insert_dados { get; set; }

        public Boolean perm_funcionario_menu { get; set; }

        public Boolean perm_funcionario_alter_dados { get; set; }

        public Boolean perm_funcionario_insert_dados { get; set; }

        public Boolean perm_fornecedores_menu { get; set; }

        public Boolean perm_fornecedores_alter_dados { get; set; }

        public Boolean perm_fornecedores_insert_dados { get; set; }

        public Boolean perm_produtos_menu { get; set; }

        public Boolean perm_produtos_alter_dados { get; set; }

        public Boolean perm_produtos_insert_dados { get; set; }

        public Boolean perm_veiculos_menu { get; set; }

        public Boolean perm_veiculos_alter_dados { get; set; }

        public Boolean perm_veiculos_insert_dados { get; set; }

        public Boolean perm_servicos_menu { get; set; }

        public Boolean perm_servicos_alter_dados { get; set; }

        public Boolean perm_servicos_insert_dados { get; set; }
    }

}
