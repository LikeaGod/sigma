﻿using MySql.Data.MySqlClient;
using SIGMA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.UsuariodoSistema.Classes.Perfil
{
    class DatabasePerfil
    {
        

        public int CriarPerfil(DTOPerfil dto)
        {
            string script = @"INSERT INTO tb_acessos(ds_nivel, ds_perfil, perm_cliente_menu, perm_cliente_alter_dados, perm_cliente_insert_dados, perm_funcionario_menu, perm_funcionario_alter_dados, perm_funcionario_insert_dados, perm_fornecedores_menu, perm_fornecedores_alter_dados, perm_fornecedores_insert_dados, perm_produtos_menu, perm_produtos_alter_dados, perm_produtos_insert_dados, perm_veiculos_menu, perm_veiculos_alter_dados, perm_veiculos_insert_dados, perm_servicos_menu, perm_servicos_alter_dados, perm_servicos_insert_dados) VALUES (@ds_nivel, @ds_perfil, @perm_cliente_menu, @perm_cliente_alter_dados, @perm_cliente_insert_dados, @perm_funcionario_menu, @perm_funcionario_alter_dados, @perm_funcionario_insert_dados, @perm_fornecedores_menu, @perm_fornecedores_alter_dados, @perm_fornecedores_insert_dados, @perm_produtos_menu, @perm_produtos_alter_dados, @perm_produtos_insert_dados, @perm_veiculos_menu, @perm_veiculos_alter_dados, @perm_veiculos_insert_dados, @perm_servicos_menu, @perm_servicos_alter_dados, @perm_servicos_insert_dados)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_nivel", dto.ds_nivel));
            parms.Add(new MySqlParameter("ds_perfil", dto.ds_perfil));
            parms.Add(new MySqlParameter("perm_cliente_menu", dto.perm_cliente_menu));
            parms.Add(new MySqlParameter("perm_cliente_alter_dados", dto.perm_cliente_alter_dados));
            parms.Add(new MySqlParameter("perm_cliente_insert_dados", dto.perm_cliente_insert_dados));
            parms.Add(new MySqlParameter("perm_fornecedores_alter_dados", dto.perm_fornecedores_alter_dados));
            parms.Add(new MySqlParameter("perm_fornecedores_insert_dados", dto.perm_fornecedores_insert_dados));
            parms.Add(new MySqlParameter("perm_fornecedores_menu", dto.perm_fornecedores_menu));
            parms.Add(new MySqlParameter("perm_funcionario_alter_dados", dto.perm_funcionario_alter_dados));
            parms.Add(new MySqlParameter("perm_funcionario_insert_dados", dto.perm_funcionario_insert_dados));
            parms.Add(new MySqlParameter("perm_funcionario_menu", dto.perm_funcionario_menu));
            parms.Add(new MySqlParameter("perm_produtos_alter_dados", dto.perm_produtos_alter_dados));
            parms.Add(new MySqlParameter("perm_produtos_insert_dados", dto.perm_produtos_insert_dados));
            parms.Add(new MySqlParameter("perm_produtos_menu", dto.perm_produtos_menu));
            parms.Add(new MySqlParameter("perm_servicos_alter_dados", dto.perm_servicos_alter_dados));
            parms.Add(new MySqlParameter("perm_servicos_insert_dados", dto.perm_servicos_insert_dados));
            parms.Add(new MySqlParameter("perm_servicos_menu", dto.perm_servicos_menu));
            parms.Add(new MySqlParameter("perm_veiculos_alter_dados", dto.perm_veiculos_alter_dados));
            parms.Add(new MySqlParameter("perm_veiculos_insert_dados", dto.perm_veiculos_insert_dados));
            parms.Add(new MySqlParameter("perm_veiculos_menu", dto.perm_veiculos_menu));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }
        public DTOPerfil Consultar_Perfis()
        {
            string script = @"select * from tb_acessos";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            DTOPerfil dto = new DTOPerfil();

            while (reader.Read())
            {
                dto.id_acessos = reader.GetInt32("id_acessos");
                dto.ds_perfil = reader.GetString("ds_perfil");
            }

            return dto;
        }
    }
}
