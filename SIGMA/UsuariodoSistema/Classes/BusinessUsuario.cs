﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.UsuariodoSistema.Classes
{
    class BusinessUsuario
    {
        public int Registrar(UsuarioDTO dto)
        {
            DatabaseUsuario dbUser = new DatabaseUsuario();
            int id = dbUser.SalvarUsuario(dto);
            return id;
        }

        public UsuarioDTO Logar(UsuarioDTO dto)
        {
            DatabaseUsuario dbUser = new DatabaseUsuario();
            dto = dbUser.Logar(dto);
            return dto;
        }
    }
}
