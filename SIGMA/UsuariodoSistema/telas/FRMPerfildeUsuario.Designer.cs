﻿namespace SIGMA.Funcionarios.telas
{
    partial class FRMPerfildeUsuario
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Nível = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNivelPSS = new System.Windows.Forms.TextBox();
            this.txtPerfilPSS = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkAMclientes = new System.Windows.Forms.CheckBox();
            this.chkINDclientes = new System.Windows.Forms.CheckBox();
            this.chkADclientes = new System.Windows.Forms.CheckBox();
            this.chkADfuncionarios = new System.Windows.Forms.CheckBox();
            this.chkAMfuncionarios = new System.Windows.Forms.CheckBox();
            this.chkINDfuncionarios = new System.Windows.Forms.CheckBox();
            this.chkAMfornecedores = new System.Windows.Forms.CheckBox();
            this.chkINDfornecedores = new System.Windows.Forms.CheckBox();
            this.chkADfornecedores = new System.Windows.Forms.CheckBox();
            this.chkAMprodutos = new System.Windows.Forms.CheckBox();
            this.chkINDprodutos = new System.Windows.Forms.CheckBox();
            this.chkADprodutos = new System.Windows.Forms.CheckBox();
            this.chkADveiculos = new System.Windows.Forms.CheckBox();
            this.chkINDveiculos = new System.Windows.Forms.CheckBox();
            this.chkAMveiculos = new System.Windows.Forms.CheckBox();
            this.chkADservicos = new System.Windows.Forms.CheckBox();
            this.chkAMservicos = new System.Windows.Forms.CheckBox();
            this.chkINDservicos = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Nível
            // 
            this.Nível.AutoSize = true;
            this.Nível.BackColor = System.Drawing.Color.Transparent;
            this.Nível.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.Nível.Location = new System.Drawing.Point(608, 19);
            this.Nível.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Nível.Name = "Nível";
            this.Nível.Size = new System.Drawing.Size(77, 29);
            this.Nível.TabIndex = 0;
            this.Nível.Text = "Nível:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label2.Location = new System.Drawing.Point(861, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Perfil:";
            // 
            // txtNivelPSS
            // 
            this.txtNivelPSS.Location = new System.Drawing.Point(684, 26);
            this.txtNivelPSS.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNivelPSS.MaxLength = 4;
            this.txtNivelPSS.Name = "txtNivelPSS";
            this.txtNivelPSS.Size = new System.Drawing.Size(141, 22);
            this.txtNivelPSS.TabIndex = 2;
            // 
            // txtPerfilPSS
            // 
            this.txtPerfilPSS.Location = new System.Drawing.Point(936, 26);
            this.txtPerfilPSS.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPerfilPSS.MaxLength = 20;
            this.txtPerfilPSS.Name = "txtPerfilPSS";
            this.txtPerfilPSS.Size = new System.Drawing.Size(125, 22);
            this.txtPerfilPSS.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(4, 85);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Clientes";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(4, 142);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Funcionários";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(4, 201);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Fornecedores";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(4, 315);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 25);
            this.label5.TabIndex = 7;
            this.label5.Text = "Veiculos";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(4, 258);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "Produtos";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(4, 372);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 25);
            this.label7.TabIndex = 9;
            this.label7.Text = "Serviços";
            // 
            // chkAMclientes
            // 
            this.chkAMclientes.AutoSize = true;
            this.chkAMclientes.BackColor = System.Drawing.Color.White;
            this.chkAMclientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAMclientes.Location = new System.Drawing.Point(9, 114);
            this.chkAMclientes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAMclientes.Name = "chkAMclientes";
            this.chkAMclientes.Size = new System.Drawing.Size(156, 24);
            this.chkAMclientes.TabIndex = 10;
            this.chkAMclientes.Text = "Acesso ao Menu";
            this.chkAMclientes.UseVisualStyleBackColor = false;
            // 
            // chkINDclientes
            // 
            this.chkINDclientes.AutoSize = true;
            this.chkINDclientes.BackColor = System.Drawing.Color.White;
            this.chkINDclientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkINDclientes.Location = new System.Drawing.Point(390, 114);
            this.chkINDclientes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkINDclientes.Name = "chkINDclientes";
            this.chkINDclientes.Size = new System.Drawing.Size(132, 24);
            this.chkINDclientes.TabIndex = 11;
            this.chkINDclientes.Text = "Inserir Dados";
            this.chkINDclientes.UseVisualStyleBackColor = false;
            // 
            // chkADclientes
            // 
            this.chkADclientes.AutoSize = true;
            this.chkADclientes.BackColor = System.Drawing.Color.White;
            this.chkADclientes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkADclientes.Location = new System.Drawing.Point(765, 114);
            this.chkADclientes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkADclientes.Name = "chkADclientes";
            this.chkADclientes.Size = new System.Drawing.Size(135, 24);
            this.chkADclientes.TabIndex = 12;
            this.chkADclientes.Text = "Alterar Dados";
            this.chkADclientes.UseVisualStyleBackColor = false;
            // 
            // chkADfuncionarios
            // 
            this.chkADfuncionarios.AutoSize = true;
            this.chkADfuncionarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkADfuncionarios.Location = new System.Drawing.Point(765, 173);
            this.chkADfuncionarios.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkADfuncionarios.Name = "chkADfuncionarios";
            this.chkADfuncionarios.Size = new System.Drawing.Size(135, 24);
            this.chkADfuncionarios.TabIndex = 13;
            this.chkADfuncionarios.Text = "Alterar Dados";
            this.chkADfuncionarios.UseVisualStyleBackColor = true;
            // 
            // chkAMfuncionarios
            // 
            this.chkAMfuncionarios.AutoSize = true;
            this.chkAMfuncionarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkAMfuncionarios.Location = new System.Drawing.Point(9, 173);
            this.chkAMfuncionarios.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAMfuncionarios.Name = "chkAMfuncionarios";
            this.chkAMfuncionarios.Size = new System.Drawing.Size(156, 24);
            this.chkAMfuncionarios.TabIndex = 14;
            this.chkAMfuncionarios.Text = "Acesso ao Menu";
            this.chkAMfuncionarios.UseVisualStyleBackColor = true;
            // 
            // chkINDfuncionarios
            // 
            this.chkINDfuncionarios.AutoSize = true;
            this.chkINDfuncionarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkINDfuncionarios.Location = new System.Drawing.Point(390, 173);
            this.chkINDfuncionarios.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkINDfuncionarios.Name = "chkINDfuncionarios";
            this.chkINDfuncionarios.Size = new System.Drawing.Size(132, 24);
            this.chkINDfuncionarios.TabIndex = 15;
            this.chkINDfuncionarios.Text = "Inserir Dados";
            this.chkINDfuncionarios.UseVisualStyleBackColor = true;
            // 
            // chkAMfornecedores
            // 
            this.chkAMfornecedores.AutoSize = true;
            this.chkAMfornecedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkAMfornecedores.Location = new System.Drawing.Point(9, 230);
            this.chkAMfornecedores.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAMfornecedores.Name = "chkAMfornecedores";
            this.chkAMfornecedores.Size = new System.Drawing.Size(156, 24);
            this.chkAMfornecedores.TabIndex = 16;
            this.chkAMfornecedores.Text = "Acesso ao Menu";
            this.chkAMfornecedores.UseVisualStyleBackColor = true;
            // 
            // chkINDfornecedores
            // 
            this.chkINDfornecedores.AutoSize = true;
            this.chkINDfornecedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkINDfornecedores.Location = new System.Drawing.Point(390, 230);
            this.chkINDfornecedores.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkINDfornecedores.Name = "chkINDfornecedores";
            this.chkINDfornecedores.Size = new System.Drawing.Size(132, 24);
            this.chkINDfornecedores.TabIndex = 17;
            this.chkINDfornecedores.Text = "Inserir Dados";
            this.chkINDfornecedores.UseVisualStyleBackColor = true;
            // 
            // chkADfornecedores
            // 
            this.chkADfornecedores.AutoSize = true;
            this.chkADfornecedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkADfornecedores.Location = new System.Drawing.Point(765, 230);
            this.chkADfornecedores.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkADfornecedores.Name = "chkADfornecedores";
            this.chkADfornecedores.Size = new System.Drawing.Size(135, 24);
            this.chkADfornecedores.TabIndex = 18;
            this.chkADfornecedores.Text = "Alterar Dados";
            this.chkADfornecedores.UseVisualStyleBackColor = true;
            // 
            // chkAMprodutos
            // 
            this.chkAMprodutos.AutoSize = true;
            this.chkAMprodutos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkAMprodutos.Location = new System.Drawing.Point(9, 287);
            this.chkAMprodutos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAMprodutos.Name = "chkAMprodutos";
            this.chkAMprodutos.Size = new System.Drawing.Size(156, 24);
            this.chkAMprodutos.TabIndex = 19;
            this.chkAMprodutos.Text = "Acesso ao Menu";
            this.chkAMprodutos.UseVisualStyleBackColor = true;
            // 
            // chkINDprodutos
            // 
            this.chkINDprodutos.AutoSize = true;
            this.chkINDprodutos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkINDprodutos.Location = new System.Drawing.Point(390, 287);
            this.chkINDprodutos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkINDprodutos.Name = "chkINDprodutos";
            this.chkINDprodutos.Size = new System.Drawing.Size(132, 24);
            this.chkINDprodutos.TabIndex = 20;
            this.chkINDprodutos.Text = "Inserir Dados";
            this.chkINDprodutos.UseVisualStyleBackColor = true;
            // 
            // chkADprodutos
            // 
            this.chkADprodutos.AutoSize = true;
            this.chkADprodutos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkADprodutos.Location = new System.Drawing.Point(765, 287);
            this.chkADprodutos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkADprodutos.Name = "chkADprodutos";
            this.chkADprodutos.Size = new System.Drawing.Size(135, 24);
            this.chkADprodutos.TabIndex = 21;
            this.chkADprodutos.Text = "Alterar Dados";
            this.chkADprodutos.UseVisualStyleBackColor = true;
            // 
            // chkADveiculos
            // 
            this.chkADveiculos.AutoSize = true;
            this.chkADveiculos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkADveiculos.Location = new System.Drawing.Point(765, 344);
            this.chkADveiculos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkADveiculos.Name = "chkADveiculos";
            this.chkADveiculos.Size = new System.Drawing.Size(135, 24);
            this.chkADveiculos.TabIndex = 22;
            this.chkADveiculos.Text = "Alterar Dados";
            this.chkADveiculos.UseVisualStyleBackColor = true;
            // 
            // chkINDveiculos
            // 
            this.chkINDveiculos.AutoSize = true;
            this.chkINDveiculos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkINDveiculos.Location = new System.Drawing.Point(390, 344);
            this.chkINDveiculos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkINDveiculos.Name = "chkINDveiculos";
            this.chkINDveiculos.Size = new System.Drawing.Size(132, 24);
            this.chkINDveiculos.TabIndex = 23;
            this.chkINDveiculos.Text = "Inserir Dados";
            this.chkINDveiculos.UseVisualStyleBackColor = true;
            // 
            // chkAMveiculos
            // 
            this.chkAMveiculos.AutoSize = true;
            this.chkAMveiculos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkAMveiculos.Location = new System.Drawing.Point(9, 344);
            this.chkAMveiculos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAMveiculos.Name = "chkAMveiculos";
            this.chkAMveiculos.Size = new System.Drawing.Size(156, 24);
            this.chkAMveiculos.TabIndex = 24;
            this.chkAMveiculos.Text = "Acesso ao Menu";
            this.chkAMveiculos.UseVisualStyleBackColor = true;
            // 
            // chkADservicos
            // 
            this.chkADservicos.AutoSize = true;
            this.chkADservicos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkADservicos.Location = new System.Drawing.Point(765, 401);
            this.chkADservicos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkADservicos.Name = "chkADservicos";
            this.chkADservicos.Size = new System.Drawing.Size(135, 24);
            this.chkADservicos.TabIndex = 25;
            this.chkADservicos.Text = "Alterar Dados";
            this.chkADservicos.UseVisualStyleBackColor = true;
            // 
            // chkAMservicos
            // 
            this.chkAMservicos.AutoSize = true;
            this.chkAMservicos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkAMservicos.Location = new System.Drawing.Point(9, 401);
            this.chkAMservicos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkAMservicos.Name = "chkAMservicos";
            this.chkAMservicos.Size = new System.Drawing.Size(156, 24);
            this.chkAMservicos.TabIndex = 27;
            this.chkAMservicos.Text = "Acesso ao Menu";
            this.chkAMservicos.UseVisualStyleBackColor = true;
            // 
            // chkINDservicos
            // 
            this.chkINDservicos.AutoSize = true;
            this.chkINDservicos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkINDservicos.Location = new System.Drawing.Point(390, 401);
            this.chkINDservicos.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkINDservicos.Name = "chkINDservicos";
            this.chkINDservicos.Size = new System.Drawing.Size(132, 24);
            this.chkINDservicos.TabIndex = 28;
            this.chkINDservicos.Text = "Inserir Dados";
            this.chkINDservicos.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(964, 395);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 43);
            this.button1.TabIndex = 29;
            this.button1.Text = "Criar Perfil";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label8.Location = new System.Drawing.Point(385, 372);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 25);
            this.label8.TabIndex = 35;
            this.label8.Text = "Serviços";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.Location = new System.Drawing.Point(385, 258);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 25);
            this.label9.TabIndex = 34;
            this.label9.Text = "Produtos";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label10.Location = new System.Drawing.Point(385, 315);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 25);
            this.label10.TabIndex = 33;
            this.label10.Text = "Veiculos";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label11.Location = new System.Drawing.Point(385, 201);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 25);
            this.label11.TabIndex = 32;
            this.label11.Text = "Fornecedores";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.Location = new System.Drawing.Point(385, 142);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(124, 25);
            this.label12.TabIndex = 31;
            this.label12.Text = "Funcionários";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label13.Location = new System.Drawing.Point(385, 85);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 25);
            this.label13.TabIndex = 30;
            this.label13.Text = "Clientes";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label14.Location = new System.Drawing.Point(760, 372);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 25);
            this.label14.TabIndex = 41;
            this.label14.Text = "Serviços";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label15.Location = new System.Drawing.Point(760, 258);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 25);
            this.label15.TabIndex = 40;
            this.label15.Text = "Produtos";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label16.Location = new System.Drawing.Point(760, 315);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 25);
            this.label16.TabIndex = 39;
            this.label16.Text = "Veiculos";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label17.Location = new System.Drawing.Point(760, 201);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 25);
            this.label17.TabIndex = 38;
            this.label17.Text = "Fornecedores";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label18.Location = new System.Drawing.Point(760, 142);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(124, 25);
            this.label18.TabIndex = 37;
            this.label18.Text = "Funcionários";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label19.Location = new System.Drawing.Point(760, 85);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 25);
            this.label19.TabIndex = 36;
            this.label19.Text = "Clientes";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label20.Location = new System.Drawing.Point(13, 12);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 39);
            this.label20.TabIndex = 42;
            this.label20.Text = "<";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // FRMPerfildeUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImage = global::SIGMA.Properties.Resources.funcionario_sem_fundo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chkINDservicos);
            this.Controls.Add(this.chkAMservicos);
            this.Controls.Add(this.chkADservicos);
            this.Controls.Add(this.chkAMveiculos);
            this.Controls.Add(this.chkINDveiculos);
            this.Controls.Add(this.chkADveiculos);
            this.Controls.Add(this.chkADprodutos);
            this.Controls.Add(this.chkINDprodutos);
            this.Controls.Add(this.chkAMprodutos);
            this.Controls.Add(this.chkADfornecedores);
            this.Controls.Add(this.chkINDfornecedores);
            this.Controls.Add(this.chkAMfornecedores);
            this.Controls.Add(this.chkINDfuncionarios);
            this.Controls.Add(this.chkAMfuncionarios);
            this.Controls.Add(this.chkADfuncionarios);
            this.Controls.Add(this.chkADclientes);
            this.Controls.Add(this.chkINDclientes);
            this.Controls.Add(this.chkAMclientes);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPerfilPSS);
            this.Controls.Add(this.txtNivelPSS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Nível);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRMPerfildeUsuario";
            this.Size = new System.Drawing.Size(1065, 441);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Nível;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNivelPSS;
        private System.Windows.Forms.TextBox txtPerfilPSS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkAMclientes;
        private System.Windows.Forms.CheckBox chkINDclientes;
        private System.Windows.Forms.CheckBox chkADclientes;
        private System.Windows.Forms.CheckBox chkADfuncionarios;
        private System.Windows.Forms.CheckBox chkINDfuncionarios;
        private System.Windows.Forms.CheckBox chkAMfornecedores;
        private System.Windows.Forms.CheckBox chkINDfornecedores;
        private System.Windows.Forms.CheckBox chkADfornecedores;
        private System.Windows.Forms.CheckBox chkAMprodutos;
        private System.Windows.Forms.CheckBox chkINDprodutos;
        private System.Windows.Forms.CheckBox chkADprodutos;
        private System.Windows.Forms.CheckBox chkADveiculos;
        private System.Windows.Forms.CheckBox chkINDveiculos;
        private System.Windows.Forms.CheckBox chkAMveiculos;
        private System.Windows.Forms.CheckBox chkADservicos;
        private System.Windows.Forms.CheckBox chkAMservicos;
        private System.Windows.Forms.CheckBox chkINDservicos;
        private System.Windows.Forms.CheckBox chkAMfuncionarios;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
    }
}
