﻿namespace SIGMA
{
    partial class FRMdeAutenticação
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRMdeAutenticação));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TXTloginUsuario = new System.Windows.Forms.TextBox();
            this.TXTloginSenha = new System.Windows.Forms.TextBox();
            this.BTNloginOK = new System.Windows.Forms.Button();
            this.BTNloginCANCELAR = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(48, 185);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Usuário:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(73, 242);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "Senha:";
            // 
            // TXTloginUsuario
            // 
            this.TXTloginUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.TXTloginUsuario.Location = new System.Drawing.Point(199, 197);
            this.TXTloginUsuario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TXTloginUsuario.Name = "TXTloginUsuario";
            this.TXTloginUsuario.Size = new System.Drawing.Size(307, 26);
            this.TXTloginUsuario.TabIndex = 2;
            // 
            // TXTloginSenha
            // 
            this.TXTloginSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.TXTloginSenha.Location = new System.Drawing.Point(193, 254);
            this.TXTloginSenha.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TXTloginSenha.Name = "TXTloginSenha";
            this.TXTloginSenha.PasswordChar = '*';
            this.TXTloginSenha.Size = new System.Drawing.Size(307, 26);
            this.TXTloginSenha.TabIndex = 3;
            this.TXTloginSenha.UseSystemPasswordChar = true;
            // 
            // BTNloginOK
            // 
            this.BTNloginOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.BTNloginOK.Location = new System.Drawing.Point(199, 334);
            this.BTNloginOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNloginOK.Name = "BTNloginOK";
            this.BTNloginOK.Size = new System.Drawing.Size(119, 49);
            this.BTNloginOK.TabIndex = 4;
            this.BTNloginOK.Text = "Ok";
            this.BTNloginOK.UseVisualStyleBackColor = true;
            this.BTNloginOK.Click += new System.EventHandler(this.BTNloginOK_Click);
            // 
            // BTNloginCANCELAR
            // 
            this.BTNloginCANCELAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.BTNloginCANCELAR.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.BTNloginCANCELAR.Location = new System.Drawing.Point(379, 334);
            this.BTNloginCANCELAR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BTNloginCANCELAR.Name = "BTNloginCANCELAR";
            this.BTNloginCANCELAR.Size = new System.Drawing.Size(127, 49);
            this.BTNloginCANCELAR.TabIndex = 5;
            this.BTNloginCANCELAR.Text = "Cancelar";
            this.BTNloginCANCELAR.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::SIGMA.Properties.Resources.Imagens_em_png_Queroimagem_com__212_;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(500, 1);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(125, 112);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(115, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(385, 48);
            this.label3.TabIndex = 7;
            this.label3.Text = "Logando no SIGMA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(304, 291);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Ainda não possui registro?";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // FRMdeAutenticação
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(625, 496);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BTNloginCANCELAR);
            this.Controls.Add(this.BTNloginOK);
            this.Controls.Add(this.TXTloginSenha);
            this.Controls.Add(this.TXTloginUsuario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRMdeAutenticação";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRMdeAutenticação";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TXTloginUsuario;
        private System.Windows.Forms.TextBox TXTloginSenha;
        private System.Windows.Forms.Button BTNloginOK;
        private System.Windows.Forms.Button BTNloginCANCELAR;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}