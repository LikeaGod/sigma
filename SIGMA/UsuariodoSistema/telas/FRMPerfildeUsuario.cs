﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIGMA.UsuariodoSistema.Classes;
using SIGMA.UsuariodoSistema.Classes.Perfil;

namespace SIGMA.Funcionarios.telas
{
    public partial class FRMPerfildeUsuario : UserControl
    {
        public FRMPerfildeUsuario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DTOPerfil dto = new DTOPerfil();
            dto.ds_nivel = txtNivelPSS.Text;
            dto.ds_perfil = txtPerfilPSS.Text ;
            dto.perm_cliente_insert_dados = chkADclientes.Checked;
            dto.perm_fornecedores_insert_dados = chkADfornecedores.Checked;
            dto.perm_funcionario_insert_dados = chkADfuncionarios.Checked;
            dto.perm_produtos_insert_dados = chkADprodutos.Checked;
            dto.perm_servicos_insert_dados = chkADservicos.Checked;
            dto.perm_veiculos_insert_dados = chkADveiculos.Checked;
            dto.perm_cliente_menu = chkAMclientes.Checked;
            dto.perm_fornecedores_menu = chkAMfornecedores.Checked;
            dto.perm_funcionario_menu = chkAMfuncionarios.Checked;
            dto.perm_produtos_menu = chkAMprodutos.Checked;
            dto.perm_servicos_menu = chkAMservicos.Checked;
            dto.perm_veiculos_menu = chkAMveiculos.Checked;
            dto.perm_cliente_alter_dados = chkINDclientes.Checked;
            dto.perm_fornecedores_alter_dados = chkINDfornecedores.Checked;
            dto.perm_funcionario_alter_dados = chkINDfuncionarios.Checked;
            dto.perm_produtos_alter_dados = chkINDprodutos.Checked;
            dto.perm_servicos_alter_dados = chkINDservicos.Checked;
            dto.perm_veiculos_alter_dados = chkINDveiculos.Checked;

            BusinessPerfil busPer = new BusinessPerfil();
            busPer.CriarPerfil(dto);
        }

        private void label20_Click(object sender, EventArgs e)
        {
            FRMcadastroDeUsuario tela = new FRMcadastroDeUsuario();
            tela.Show();

            
        }
    }
}
