﻿namespace SIGMA
{
    partial class FRMcadastroDeUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FRMcadastroDeUsuario));
            this.btnUserPesquisar = new System.Windows.Forms.PictureBox();
            this.BTNcadUserALTERAR = new System.Windows.Forms.Button();
            this.BTNcadUserINSERIR = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.BTNcadUserFECHAR = new System.Windows.Forms.Button();
            this.BTNcadUserSALVAR = new System.Windows.Forms.Button();
            this.BTNcadUserEXCLUIR = new System.Windows.Forms.Button();
            this.BTNcadUserCANCELAR = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.PanelcentroFunc = new System.Windows.Forms.Panel();
            this.LBLnumeroCodigo = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cboUserDepartamento = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.RDNautoriNAO = new System.Windows.Forms.RadioButton();
            this.RDNautoriSIM = new System.Windows.Forms.RadioButton();
            this.CBOcaduserPerfil = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TXTcaduserOBSERVAÇÃO = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TXTcaduserConfimarSenha = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TXTcaduserSenha = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNM_User = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnUserPesquisar)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.PanelcentroFunc.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnUserPesquisar
            // 
            this.btnUserPesquisar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUserPesquisar.BackgroundImage")));
            this.btnUserPesquisar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnUserPesquisar.Location = new System.Drawing.Point(-4, 0);
            this.btnUserPesquisar.Margin = new System.Windows.Forms.Padding(4);
            this.btnUserPesquisar.Name = "btnUserPesquisar";
            this.btnUserPesquisar.Size = new System.Drawing.Size(97, 49);
            this.btnUserPesquisar.TabIndex = 0;
            this.btnUserPesquisar.TabStop = false;
            // 
            // BTNcadUserALTERAR
            // 
            this.BTNcadUserALTERAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNcadUserALTERAR.Location = new System.Drawing.Point(137, 9);
            this.BTNcadUserALTERAR.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcadUserALTERAR.Name = "BTNcadUserALTERAR";
            this.BTNcadUserALTERAR.Size = new System.Drawing.Size(119, 44);
            this.BTNcadUserALTERAR.TabIndex = 1;
            this.BTNcadUserALTERAR.Text = "Alterar";
            this.BTNcadUserALTERAR.UseVisualStyleBackColor = true;
            // 
            // BTNcadUserINSERIR
            // 
            this.BTNcadUserINSERIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNcadUserINSERIR.Location = new System.Drawing.Point(13, 9);
            this.BTNcadUserINSERIR.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcadUserINSERIR.Name = "BTNcadUserINSERIR";
            this.BTNcadUserINSERIR.Size = new System.Drawing.Size(116, 44);
            this.BTNcadUserINSERIR.TabIndex = 0;
            this.BTNcadUserINSERIR.Text = "Inserir";
            this.BTNcadUserINSERIR.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.BTNcadUserFECHAR);
            this.panel1.Controls.Add(this.BTNcadUserSALVAR);
            this.panel1.Controls.Add(this.BTNcadUserEXCLUIR);
            this.panel1.Controls.Add(this.BTNcadUserCANCELAR);
            this.panel1.Controls.Add(this.BTNcadUserALTERAR);
            this.panel1.Controls.Add(this.BTNcadUserINSERIR);
            this.panel1.Location = new System.Drawing.Point(0, 490);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1065, 66);
            this.panel1.TabIndex = 22;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(519, 9);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 44);
            this.button1.TabIndex = 6;
            this.button1.Text = "Perfil";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BTNcadUserFECHAR
            // 
            this.BTNcadUserFECHAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNcadUserFECHAR.Location = new System.Drawing.Point(935, 9);
            this.BTNcadUserFECHAR.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcadUserFECHAR.Name = "BTNcadUserFECHAR";
            this.BTNcadUserFECHAR.Size = new System.Drawing.Size(119, 44);
            this.BTNcadUserFECHAR.TabIndex = 5;
            this.BTNcadUserFECHAR.Text = "Fechar";
            this.BTNcadUserFECHAR.UseVisualStyleBackColor = true;
            // 
            // BTNcadUserSALVAR
            // 
            this.BTNcadUserSALVAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNcadUserSALVAR.Location = new System.Drawing.Point(808, 9);
            this.BTNcadUserSALVAR.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcadUserSALVAR.Name = "BTNcadUserSALVAR";
            this.BTNcadUserSALVAR.Size = new System.Drawing.Size(119, 44);
            this.BTNcadUserSALVAR.TabIndex = 4;
            this.BTNcadUserSALVAR.Text = "Salvar";
            this.BTNcadUserSALVAR.UseVisualStyleBackColor = true;
            this.BTNcadUserSALVAR.Click += new System.EventHandler(this.BTNcadUserSALVAR_Click);
            // 
            // BTNcadUserEXCLUIR
            // 
            this.BTNcadUserEXCLUIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNcadUserEXCLUIR.Location = new System.Drawing.Point(391, 9);
            this.BTNcadUserEXCLUIR.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcadUserEXCLUIR.Name = "BTNcadUserEXCLUIR";
            this.BTNcadUserEXCLUIR.Size = new System.Drawing.Size(119, 44);
            this.BTNcadUserEXCLUIR.TabIndex = 3;
            this.BTNcadUserEXCLUIR.Text = "Excluir";
            this.BTNcadUserEXCLUIR.UseVisualStyleBackColor = true;
            // 
            // BTNcadUserCANCELAR
            // 
            this.BTNcadUserCANCELAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNcadUserCANCELAR.Location = new System.Drawing.Point(264, 9);
            this.BTNcadUserCANCELAR.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcadUserCANCELAR.Name = "BTNcadUserCANCELAR";
            this.BTNcadUserCANCELAR.Size = new System.Drawing.Size(119, 44);
            this.BTNcadUserCANCELAR.TabIndex = 2;
            this.BTNcadUserCANCELAR.Text = "Cancelar";
            this.BTNcadUserCANCELAR.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.btnUserPesquisar);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1065, 49);
            this.panel2.TabIndex = 23;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Location = new System.Drawing.Point(101, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(95, 49);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // PanelcentroFunc
            // 
            this.PanelcentroFunc.BackColor = System.Drawing.Color.WhiteSmoke;
            this.PanelcentroFunc.BackgroundImage = global::SIGMA.Properties.Resources.funcionario_sem_fundo;
            this.PanelcentroFunc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PanelcentroFunc.Controls.Add(this.LBLnumeroCodigo);
            this.PanelcentroFunc.Controls.Add(this.label10);
            this.PanelcentroFunc.Controls.Add(this.cboUserDepartamento);
            this.PanelcentroFunc.Controls.Add(this.label9);
            this.PanelcentroFunc.Controls.Add(this.RDNautoriNAO);
            this.PanelcentroFunc.Controls.Add(this.RDNautoriSIM);
            this.PanelcentroFunc.Controls.Add(this.CBOcaduserPerfil);
            this.PanelcentroFunc.Controls.Add(this.label8);
            this.PanelcentroFunc.Controls.Add(this.TXTcaduserOBSERVAÇÃO);
            this.PanelcentroFunc.Controls.Add(this.label7);
            this.PanelcentroFunc.Controls.Add(this.label6);
            this.PanelcentroFunc.Controls.Add(this.TXTcaduserConfimarSenha);
            this.PanelcentroFunc.Controls.Add(this.label5);
            this.PanelcentroFunc.Controls.Add(this.TXTcaduserSenha);
            this.PanelcentroFunc.Controls.Add(this.label3);
            this.PanelcentroFunc.Controls.Add(this.label4);
            this.PanelcentroFunc.Controls.Add(this.txtNM_User);
            this.PanelcentroFunc.Controls.Add(this.txtNome);
            this.PanelcentroFunc.Controls.Add(this.label2);
            this.PanelcentroFunc.Controls.Add(this.label1);
            this.PanelcentroFunc.Location = new System.Drawing.Point(0, 50);
            this.PanelcentroFunc.Margin = new System.Windows.Forms.Padding(4);
            this.PanelcentroFunc.Name = "PanelcentroFunc";
            this.PanelcentroFunc.Size = new System.Drawing.Size(1065, 441);
            this.PanelcentroFunc.TabIndex = 24;
            // 
            // LBLnumeroCodigo
            // 
            this.LBLnumeroCodigo.AutoSize = true;
            this.LBLnumeroCodigo.BackColor = System.Drawing.Color.Transparent;
            this.LBLnumeroCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLnumeroCodigo.ForeColor = System.Drawing.Color.Black;
            this.LBLnumeroCodigo.Location = new System.Drawing.Point(341, 53);
            this.LBLnumeroCodigo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLnumeroCodigo.Name = "LBLnumeroCodigo";
            this.LBLnumeroCodigo.Size = new System.Drawing.Size(29, 31);
            this.LBLnumeroCodigo.TabIndex = 41;
            this.LBLnumeroCodigo.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(225, 53);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 31);
            this.label10.TabIndex = 40;
            this.label10.Text = "Codigo";
            // 
            // cboUserDepartamento
            // 
            this.cboUserDepartamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUserDepartamento.FormattingEnabled = true;
            this.cboUserDepartamento.Location = new System.Drawing.Point(827, 41);
            this.cboUserDepartamento.Margin = new System.Windows.Forms.Padding(4);
            this.cboUserDepartamento.Name = "cboUserDepartamento";
            this.cboUserDepartamento.Size = new System.Drawing.Size(231, 24);
            this.cboUserDepartamento.TabIndex = 38;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(625, 34);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(194, 31);
            this.label9.TabIndex = 39;
            this.label9.Text = "Departamento:";
            // 
            // RDNautoriNAO
            // 
            this.RDNautoriNAO.AutoSize = true;
            this.RDNautoriNAO.BackColor = System.Drawing.Color.Transparent;
            this.RDNautoriNAO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RDNautoriNAO.ForeColor = System.Drawing.Color.Black;
            this.RDNautoriNAO.Location = new System.Drawing.Point(774, 231);
            this.RDNautoriNAO.Margin = new System.Windows.Forms.Padding(4);
            this.RDNautoriNAO.Name = "RDNautoriNAO";
            this.RDNautoriNAO.Size = new System.Drawing.Size(69, 29);
            this.RDNautoriNAO.TabIndex = 37;
            this.RDNautoriNAO.TabStop = true;
            this.RDNautoriNAO.Text = "Não";
            this.RDNautoriNAO.UseVisualStyleBackColor = false;
            // 
            // RDNautoriSIM
            // 
            this.RDNautoriSIM.AutoSize = true;
            this.RDNautoriSIM.BackColor = System.Drawing.Color.Transparent;
            this.RDNautoriSIM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.RDNautoriSIM.ForeColor = System.Drawing.Color.Black;
            this.RDNautoriSIM.Location = new System.Drawing.Point(645, 231);
            this.RDNautoriSIM.Margin = new System.Windows.Forms.Padding(4);
            this.RDNautoriSIM.Name = "RDNautoriSIM";
            this.RDNautoriSIM.Size = new System.Drawing.Size(67, 29);
            this.RDNautoriSIM.TabIndex = 36;
            this.RDNautoriSIM.TabStop = true;
            this.RDNautoriSIM.Text = "Sim";
            this.RDNautoriSIM.UseVisualStyleBackColor = false;
            // 
            // CBOcaduserPerfil
            // 
            this.CBOcaduserPerfil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBOcaduserPerfil.FormattingEnabled = true;
            this.CBOcaduserPerfil.Location = new System.Drawing.Point(327, 161);
            this.CBOcaduserPerfil.Margin = new System.Windows.Forms.Padding(4);
            this.CBOcaduserPerfil.Name = "CBOcaduserPerfil";
            this.CBOcaduserPerfil.Size = new System.Drawing.Size(311, 24);
            this.CBOcaduserPerfil.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(638, 194);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(217, 31);
            this.label8.TabIndex = 35;
            this.label8.Text = "Autorizar acesso";
            // 
            // TXTcaduserOBSERVAÇÃO
            // 
            this.TXTcaduserOBSERVAÇÃO.Location = new System.Drawing.Point(4, 326);
            this.TXTcaduserOBSERVAÇÃO.Margin = new System.Windows.Forms.Padding(4);
            this.TXTcaduserOBSERVAÇÃO.Multiline = true;
            this.TXTcaduserOBSERVAÇÃO.Name = "TXTcaduserOBSERVAÇÃO";
            this.TXTcaduserOBSERVAÇÃO.Size = new System.Drawing.Size(583, 111);
            this.TXTcaduserOBSERVAÇÃO.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(4, 291);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 31);
            this.label7.TabIndex = 33;
            this.label7.Text = "Observação:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(5, 2);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(180, 31);
            this.label6.TabIndex = 32;
            this.label6.Text = "Dados Gerais";
            // 
            // TXTcaduserConfimarSenha
            // 
            this.TXTcaduserConfimarSenha.Location = new System.Drawing.Point(327, 225);
            this.TXTcaduserConfimarSenha.Margin = new System.Windows.Forms.Padding(4);
            this.TXTcaduserConfimarSenha.Name = "TXTcaduserConfimarSenha";
            this.TXTcaduserConfimarSenha.PasswordChar = '*';
            this.TXTcaduserConfimarSenha.Size = new System.Drawing.Size(192, 22);
            this.TXTcaduserConfimarSenha.TabIndex = 31;
            this.TXTcaduserConfimarSenha.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(99, 215);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(226, 31);
            this.label5.TabIndex = 30;
            this.label5.Text = "Confirmar Senha:";
            // 
            // TXTcaduserSenha
            // 
            this.TXTcaduserSenha.Location = new System.Drawing.Point(327, 194);
            this.TXTcaduserSenha.Margin = new System.Windows.Forms.Padding(4);
            this.TXTcaduserSenha.Name = "TXTcaduserSenha";
            this.TXTcaduserSenha.PasswordChar = '*';
            this.TXTcaduserSenha.Size = new System.Drawing.Size(192, 22);
            this.TXTcaduserSenha.TabIndex = 29;
            this.TXTcaduserSenha.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(225, 184);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 31);
            this.label3.TabIndex = 28;
            this.label3.Text = "Senha:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(241, 151);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 31);
            this.label4.TabIndex = 27;
            this.label4.Text = "Perfil:";
            // 
            // txtNM_User
            // 
            this.txtNM_User.Location = new System.Drawing.Point(327, 130);
            this.txtNM_User.Margin = new System.Windows.Forms.Padding(4);
            this.txtNM_User.Name = "txtNM_User";
            this.txtNM_User.Size = new System.Drawing.Size(311, 22);
            this.txtNM_User.TabIndex = 26;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(327, 98);
            this.txtNome.Margin = new System.Windows.Forms.Padding(4);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(311, 22);
            this.txtNome.TabIndex = 25;
            this.txtNome.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(209, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 31);
            this.label2.TabIndex = 24;
            this.label2.Text = "Usuário:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(231, 88);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 31);
            this.label1.TabIndex = 22;
            this.label1.Text = "Nome:";
            // 
            // FRMcadastroDeUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SIGMA.Properties.Resources.awesome_car_background_awesome_car_background_awesome_car_background_pictures_awesome_car_backgrounds_awesome_cool_car_728x410;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.PanelcentroFunc);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRMcadastroDeUsuario";
            this.Text = "FRMcadastroDeUsuario";
            ((System.ComponentModel.ISupportInitialize)(this.btnUserPesquisar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.PanelcentroFunc.ResumeLayout(false);
            this.PanelcentroFunc.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox btnUserPesquisar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BTNcadUserFECHAR;
        private System.Windows.Forms.Button BTNcadUserSALVAR;
        private System.Windows.Forms.Button BTNcadUserEXCLUIR;
        private System.Windows.Forms.Button BTNcadUserCANCELAR;
        private System.Windows.Forms.Button BTNcadUserALTERAR;
        private System.Windows.Forms.Button BTNcadUserINSERIR;
        private System.Windows.Forms.Panel PanelcentroFunc;
        private System.Windows.Forms.Label LBLnumeroCodigo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboUserDepartamento;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton RDNautoriNAO;
        private System.Windows.Forms.RadioButton RDNautoriSIM;
        private System.Windows.Forms.ComboBox CBOcaduserPerfil;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TXTcaduserOBSERVAÇÃO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TXTcaduserConfimarSenha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TXTcaduserSenha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNM_User;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button1;
    }
}