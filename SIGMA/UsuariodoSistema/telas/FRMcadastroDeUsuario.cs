﻿using SIGMA.Funcionarios.telas;
using SIGMA.UsuariodoSistema.Classes;
using SIGMA.UsuariodoSistema.Classes.Perfil;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA
{
    public partial class FRMcadastroDeUsuario : Form
    {
        public FRMcadastroDeUsuario()
        {
            InitializeComponent();
            CarregarCombos();
        }

        BusinessPerfil perfil = new BusinessPerfil();
        DTOPerfil dtoSTRINGS_IDS = new DTOPerfil();

        public void CarregarCombos()
        {
          dtoSTRINGS_IDS = perfil.Consultar_Perfil();

            List<string> perfis = new List<string>();
            perfis.Add(dtoSTRINGS_IDS.ds_perfil);
            CBOcaduserPerfil.DataSource = perfis;
        }

        private void BTNcadUserSALVAR_Click(object sender, EventArgs e)
        {
            UsuarioDTO dto = new UsuarioDTO();
            if (RDNautoriSIM.Checked == true)
            {
                dto.Acesso_Autorizado = true;
            }
            else if (RDNautoriNAO.Checked == true)
            {
                dto.Acesso_Autorizado = false;
            }
            dto.Nome = txtNome.Text.Trim();
            dto.Nm_Usuario = txtNM_User.Text.Trim();
            dto.Observacao = TXTcaduserOBSERVAÇÃO.Text.Trim();
            dto.Senha = TXTcaduserSenha.Text.Trim();
            dto.Confirmar_Senha = TXTcaduserConfimarSenha.Text.Trim();

            BusinessUsuario bus = new BusinessUsuario();
            int id = bus.Registrar(dto);

            MessageBox.Show("Registrado com sucesso");

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            PanelcentroFunc.Controls.Clear();
            FRMPerfildeUsuario perfil = new FRMPerfildeUsuario();

            if (PanelcentroFunc.Controls.Count > 5)
            {
                PanelcentroFunc.Controls.RemoveAt(PanelcentroFunc.Controls.Count -1);
            }
            PanelcentroFunc.Controls.Add(perfil);
        }

        
    }
}
