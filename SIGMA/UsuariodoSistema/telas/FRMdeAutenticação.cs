﻿using SIGMA.UsuariodoSistema.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA
{
    public partial class FRMdeAutenticação : Form
    {
        public FRMdeAutenticação()
        {
            InitializeComponent();
        }

        private void BTNloginOK_Click(object sender, EventArgs e)
        {
            UsuarioDTO dto = new UsuarioDTO();

            BusinessUsuario bus = new BusinessUsuario();
            dto.Nm_Usuario = TXTloginUsuario.Text;
            dto.Senha = TXTloginSenha.Text;


            dto = bus.Logar(dto);
            bool entrar = dto.Acesso_Autorizado;
            
            if ( entrar == true)
            {
                SessaoUsuarioLogado.Logado = dto;

                MessageBox.Show("Login efetuado com sucesso", "Sistema" , MessageBoxButtons.OK , MessageBoxIcon.Information);

                FRMsigma tela = new FRMsigma();
                tela.Show();

                this.Hide();
            }

            else if(dto.Acesso_Autorizado == false)
            {
                MessageBox.Show("Credênciais Inválidas ou Autorização Negada", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void label4_Click(object sender, EventArgs e)
        {
            FRMcadastroDeUsuario registro = new FRMcadastroDeUsuario();
            registro.Show();
            this.Hide();
        }
    }
}
