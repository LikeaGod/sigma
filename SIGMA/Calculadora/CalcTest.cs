﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA.Calculadora
{
    public partial class CalcTest : Form
    {
        public CalcTest()
        {
            InitializeComponent();
        }

        private void btndivi_Click(object sender, EventArgs e)
        {

            decimal vl1 = Convert.ToDecimal(lblVl1.Text);
            decimal vl2 = Convert.ToDecimal(lblVl2.Text);
            
            decimal resultado = vl1 / vl2;

            label1.Text = resultado.ToString();
        }

        private void btnAdic_Click(object sender, EventArgs e)
        {
            int vl1 = Convert.ToInt32(lblVl1.Text);
            int vl2 = Convert.ToInt32(lblVl2.Text);

            int resultado = vl1 + vl2;

            label1.Text = resultado.ToString();

        }

        private void btnSub_Click(object sender, EventArgs e)
        {

            decimal vl1 = Convert.ToDecimal(lblVl1.Text);
            decimal vl2 = Convert.ToDecimal(lblVl2.Text);
            
            decimal resultado = vl1 - vl2;

            label1.Text = resultado.ToString();
        }

        private void btnMult_Click(object sender, EventArgs e)
        {

            int vl1 = Convert.ToInt32(lblVl1.Text);
            int vl2 = Convert.ToInt32(lblVl2.Text);

            int resultado = vl1 * vl2;

            label1.Text = resultado.ToString();
        }

        private void btnPotencia_Click(object sender, EventArgs e)
        {

            double vl1 = Convert.ToDouble(lblVl1.Text);
            double vl2 = Convert.ToDouble(lblVl2.Text);

            double resultado =Math.Pow(vl1,vl2);

            label1.Text = resultado.ToString();
        }

        private void btnPerCent_Click(object sender, EventArgs e)
        {

        }
    }
}
