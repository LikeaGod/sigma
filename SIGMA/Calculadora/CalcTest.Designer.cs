﻿namespace SIGMA.Calculadora
{
    partial class CalcTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btndivi = new System.Windows.Forms.Button();
            this.btnPotencia = new System.Windows.Forms.Button();
            this.btnAdic = new System.Windows.Forms.Button();
            this.btnSub = new System.Windows.Forms.Button();
            this.btnMult = new System.Windows.Forms.Button();
            this.lblVl1 = new System.Windows.Forms.TextBox();
            this.lblVl2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btndivi
            // 
            this.btndivi.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btndivi.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btndivi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btndivi.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btndivi.Location = new System.Drawing.Point(46, 241);
            this.btndivi.Name = "btndivi";
            this.btndivi.Size = new System.Drawing.Size(83, 88);
            this.btndivi.TabIndex = 36;
            this.btndivi.Text = "÷";
            this.btndivi.UseVisualStyleBackColor = true;
            this.btndivi.Click += new System.EventHandler(this.btndivi_Click);
            // 
            // btnPotencia
            // 
            this.btnPotencia.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnPotencia.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPotencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPotencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.btnPotencia.Location = new System.Drawing.Point(87, 335);
            this.btnPotencia.Name = "btnPotencia";
            this.btnPotencia.Size = new System.Drawing.Size(83, 88);
            this.btnPotencia.TabIndex = 35;
            this.btnPotencia.Text = "^";
            this.btnPotencia.UseVisualStyleBackColor = true;
            this.btnPotencia.Click += new System.EventHandler(this.btnPotencia_Click);
            // 
            // btnAdic
            // 
            this.btnAdic.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnAdic.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAdic.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdic.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btnAdic.Location = new System.Drawing.Point(135, 241);
            this.btnAdic.Name = "btnAdic";
            this.btnAdic.Size = new System.Drawing.Size(83, 88);
            this.btnAdic.TabIndex = 33;
            this.btnAdic.Text = "+";
            this.btnAdic.UseVisualStyleBackColor = true;
            this.btnAdic.Click += new System.EventHandler(this.btnAdic_Click);
            // 
            // btnSub
            // 
            this.btnSub.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSub.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSub.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btnSub.Location = new System.Drawing.Point(224, 241);
            this.btnSub.Name = "btnSub";
            this.btnSub.Size = new System.Drawing.Size(83, 88);
            this.btnSub.TabIndex = 32;
            this.btnSub.Text = "-";
            this.btnSub.UseVisualStyleBackColor = true;
            this.btnSub.Click += new System.EventHandler(this.btnSub_Click);
            // 
            // btnMult
            // 
            this.btnMult.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnMult.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMult.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.btnMult.Location = new System.Drawing.Point(176, 335);
            this.btnMult.Name = "btnMult";
            this.btnMult.Size = new System.Drawing.Size(83, 88);
            this.btnMult.TabIndex = 31;
            this.btnMult.Text = "X";
            this.btnMult.UseVisualStyleBackColor = true;
            this.btnMult.Click += new System.EventHandler(this.btnMult_Click);
            // 
            // lblVl1
            // 
            this.lblVl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVl1.Location = new System.Drawing.Point(12, 12);
            this.lblVl1.Name = "lblVl1";
            this.lblVl1.Size = new System.Drawing.Size(333, 55);
            this.lblVl1.TabIndex = 19;
            // 
            // lblVl2
            // 
            this.lblVl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVl2.Location = new System.Drawing.Point(12, 73);
            this.lblVl2.Name = "lblVl2";
            this.lblVl2.Size = new System.Drawing.Size(333, 55);
            this.lblVl2.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(67, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 51);
            this.label1.TabIndex = 39;
            this.label1.Text = "Resultado";
            // 
            // CalcTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 435);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblVl2);
            this.Controls.Add(this.btndivi);
            this.Controls.Add(this.btnPotencia);
            this.Controls.Add(this.btnAdic);
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.btnMult);
            this.Controls.Add(this.lblVl1);
            this.Name = "CalcTest";
            this.Text = "CalcTest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btndivi;
        private System.Windows.Forms.Button btnPotencia;
        private System.Windows.Forms.Button btnAdic;
        private System.Windows.Forms.Button btnSub;
        private System.Windows.Forms.Button btnMult;
        private System.Windows.Forms.TextBox lblVl1;
        private System.Windows.Forms.TextBox lblVl2;
        private System.Windows.Forms.Label label1;
    }
}