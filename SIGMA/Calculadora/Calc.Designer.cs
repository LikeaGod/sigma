﻿namespace SIGMA.Calculadora
{
    partial class Calc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblConta = new System.Windows.Forms.TextBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnVir = new System.Windows.Forms.Button();
            this.btnMult = new System.Windows.Forms.Button();
            this.btnSub = new System.Windows.Forms.Button();
            this.btnAdic = new System.Windows.Forms.Button();
            this.btnresu = new System.Windows.Forms.Button();
            this.btnPotencia = new System.Windows.Forms.Button();
            this.btndivi = new System.Windows.Forms.Button();
            this.btnPerCent = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblConta
            // 
            this.lblConta.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F);
            this.lblConta.Location = new System.Drawing.Point(12, 12);
            this.lblConta.Name = "lblConta";
            this.lblConta.Size = new System.Drawing.Size(333, 74);
            this.lblConta.TabIndex = 0;
            this.lblConta.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btn1
            // 
            this.btn1.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn1.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn1.Location = new System.Drawing.Point(12, 113);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(53, 75);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn2
            // 
            this.btn2.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn2.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn2.Location = new System.Drawing.Point(71, 113);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(53, 75);
            this.btn2.TabIndex = 2;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn3
            // 
            this.btn3.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn3.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn3.Location = new System.Drawing.Point(130, 113);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(53, 75);
            this.btn3.TabIndex = 3;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn6
            // 
            this.btn6.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn6.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn6.Location = new System.Drawing.Point(130, 194);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(53, 75);
            this.btn6.TabIndex = 6;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.button4_Click);
            // 
            // btn5
            // 
            this.btn5.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn5.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn5.Location = new System.Drawing.Point(71, 194);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(53, 75);
            this.btn5.TabIndex = 5;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btn4
            // 
            this.btn4.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn4.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn4.Location = new System.Drawing.Point(12, 194);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(53, 75);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.button6_Click);
            // 
            // btn9
            // 
            this.btn9.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn9.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn9.Location = new System.Drawing.Point(130, 275);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(53, 75);
            this.btn9.TabIndex = 9;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.button7_Click);
            // 
            // btn8
            // 
            this.btn8.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn8.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn8.Location = new System.Drawing.Point(71, 275);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(53, 75);
            this.btn8.TabIndex = 8;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btn7
            // 
            this.btn7.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn7.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn7.Location = new System.Drawing.Point(12, 275);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(53, 75);
            this.btn7.TabIndex = 7;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.button9_Click);
            // 
            // btn0
            // 
            this.btn0.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btn0.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btn0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn0.Location = new System.Drawing.Point(12, 356);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(171, 66);
            this.btn0.TabIndex = 10;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.button10_Click);
            // 
            // btnVir
            // 
            this.btnVir.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnVir.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnVir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnVir.Location = new System.Drawing.Point(189, 356);
            this.btnVir.Name = "btnVir";
            this.btnVir.Size = new System.Drawing.Size(75, 66);
            this.btnVir.TabIndex = 11;
            this.btnVir.Text = ",";
            this.btnVir.UseVisualStyleBackColor = true;
            this.btnVir.Click += new System.EventHandler(this.button11_Click);
            // 
            // btnMult
            // 
            this.btnMult.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnMult.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnMult.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.btnMult.Location = new System.Drawing.Point(270, 113);
            this.btnMult.Name = "btnMult";
            this.btnMult.Size = new System.Drawing.Size(75, 75);
            this.btnMult.TabIndex = 12;
            this.btnMult.Text = "X";
            this.btnMult.UseVisualStyleBackColor = true;
            this.btnMult.Click += new System.EventHandler(this.button12_Click);
            // 
            // btnSub
            // 
            this.btnSub.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSub.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSub.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btnSub.Location = new System.Drawing.Point(270, 194);
            this.btnSub.Name = "btnSub";
            this.btnSub.Size = new System.Drawing.Size(75, 75);
            this.btnSub.TabIndex = 13;
            this.btnSub.Text = "-";
            this.btnSub.UseVisualStyleBackColor = true;
            this.btnSub.Click += new System.EventHandler(this.button13_Click);
            // 
            // btnAdic
            // 
            this.btnAdic.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnAdic.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAdic.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdic.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btnAdic.Location = new System.Drawing.Point(270, 275);
            this.btnAdic.Name = "btnAdic";
            this.btnAdic.Size = new System.Drawing.Size(75, 75);
            this.btnAdic.TabIndex = 14;
            this.btnAdic.Text = "+";
            this.btnAdic.UseVisualStyleBackColor = true;
            // 
            // btnresu
            // 
            this.btnresu.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnresu.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnresu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnresu.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btnresu.Location = new System.Drawing.Point(270, 356);
            this.btnresu.Name = "btnresu";
            this.btnresu.Size = new System.Drawing.Size(75, 66);
            this.btnresu.TabIndex = 15;
            this.btnresu.Text = "=";
            this.btnresu.UseVisualStyleBackColor = true;
            // 
            // btnPotencia
            // 
            this.btnPotencia.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnPotencia.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPotencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPotencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.btnPotencia.Location = new System.Drawing.Point(189, 275);
            this.btnPotencia.Name = "btnPotencia";
            this.btnPotencia.Size = new System.Drawing.Size(75, 75);
            this.btnPotencia.TabIndex = 16;
            this.btnPotencia.Text = "^";
            this.btnPotencia.UseVisualStyleBackColor = true;
            // 
            // btndivi
            // 
            this.btndivi.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btndivi.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btndivi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btndivi.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.btndivi.Location = new System.Drawing.Point(189, 194);
            this.btndivi.Name = "btndivi";
            this.btndivi.Size = new System.Drawing.Size(75, 75);
            this.btndivi.TabIndex = 17;
            this.btndivi.Text = "÷";
            this.btndivi.UseVisualStyleBackColor = true;
            // 
            // btnPerCent
            // 
            this.btnPerCent.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlLight;
            this.btnPerCent.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPerCent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPerCent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.btnPerCent.Location = new System.Drawing.Point(189, 113);
            this.btnPerCent.Name = "btnPerCent";
            this.btnPerCent.Size = new System.Drawing.Size(75, 75);
            this.btnPerCent.TabIndex = 18;
            this.btnPerCent.Text = "%";
            this.btnPerCent.UseVisualStyleBackColor = true;
            // 
            // Calc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 433);
            this.Controls.Add(this.btnPerCent);
            this.Controls.Add(this.btndivi);
            this.Controls.Add(this.btnPotencia);
            this.Controls.Add(this.btnresu);
            this.Controls.Add(this.btnAdic);
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.btnMult);
            this.Controls.Add(this.btnVir);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.lblConta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Calc";
            this.Load += new System.EventHandler(this.Calc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox lblConta;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnVir;
        private System.Windows.Forms.Button btnMult;
        private System.Windows.Forms.Button btnSub;
        private System.Windows.Forms.Button btnAdic;
        private System.Windows.Forms.Button btnresu;
        private System.Windows.Forms.Button btnPotencia;
        private System.Windows.Forms.Button btndivi;
        private System.Windows.Forms.Button btnPerCent;
    }
}