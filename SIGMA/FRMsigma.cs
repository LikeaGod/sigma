﻿using SIGMA.Clientes;
using SIGMA.Clientes.telas;
using SIGMA.Funcionarios;
using SIGMA.Veiculos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA
{
    public partial class FRMsigma : Form
    {
        public FRMsigma()
        {
            InitializeComponent();
        }

        private void BTNveiculos_Click(object sender, EventArgs e)
        {
            panelCentrofrmSIGMA.Controls.Clear();
            frmLocalizarModelo localizar = new frmLocalizarModelo();
            if (panelCentrofrmSIGMA.Controls.Count > 5)
            {
                panelCentrofrmSIGMA.Controls.RemoveAt(panelCentrofrmSIGMA.Controls.Count - 1);
            }
            panelCentrofrmSIGMA.Controls.Add(localizar);

        }

        private void BTNcalculadora_Click(object sender, EventArgs e)
        {

        }

        private void BTNclientes_Click(object sender, EventArgs e)
        {
            panelCentrofrmSIGMA.Controls.Clear();
            FRMlocalizarCliente clientetela = new FRMlocalizarCliente();
            panelCentrofrmSIGMA.Controls.Add(clientetela);
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            FRMcadastroCliente cadastCliente = new FRMcadastroCliente();
            cadastCliente.Show();
        }

        private void fornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void funcionáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            frnCadastrodeFuncionarios cadastFunc = new frnCadastrodeFuncionarios();
            cadastFunc.Show();
        }

        private void veículosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            frmCadastrodeVeiculos veic = new frmCadastrodeVeiculos();
            veic.Show();
        }
    }
}
