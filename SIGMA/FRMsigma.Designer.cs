﻿namespace SIGMA
{
    partial class FRMsigma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.veículosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviçosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marcasEModelosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoffDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.movimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relatóriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utilitáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BTNsair = new System.Windows.Forms.Button();
            this.BTNcalendario = new System.Windows.Forms.Button();
            this.BTNcalculadora = new System.Windows.Forms.Button();
            this.BTNordemServico = new System.Windows.Forms.Button();
            this.BTNorcamento = new System.Windows.Forms.Button();
            this.BTNprodutos = new System.Windows.Forms.Button();
            this.BTNveiculos = new System.Windows.Forms.Button();
            this.BTNfuncionarios = new System.Windows.Forms.Button();
            this.BTNfornecedores = new System.Windows.Forms.Button();
            this.BTNclientes = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSPdata = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSPnmUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelCentrofrmSIGMA = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroToolStripMenuItem,
            this.movimentoToolStripMenuItem,
            this.relatóriosToolStripMenuItem,
            this.utilitáriosToolStripMenuItem,
            this.ajudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1265, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clientesToolStripMenuItem,
            this.fornecedoresToolStripMenuItem,
            this.funcionáriosToolStripMenuItem,
            this.veículosToolStripMenuItem,
            this.produtosToolStripMenuItem,
            this.serviçosToolStripMenuItem,
            this.marcasEModelosToolStripMenuItem,
            this.unidadesToolStripMenuItem,
            this.logoffDeToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(80, 24);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.clientesToolStripMenuItem.Text = "Clientes";
            this.clientesToolStripMenuItem.Click += new System.EventHandler(this.clientesToolStripMenuItem_Click);
            // 
            // fornecedoresToolStripMenuItem
            // 
            this.fornecedoresToolStripMenuItem.Name = "fornecedoresToolStripMenuItem";
            this.fornecedoresToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.fornecedoresToolStripMenuItem.Text = "Fornecedores";
            this.fornecedoresToolStripMenuItem.Click += new System.EventHandler(this.fornecedoresToolStripMenuItem_Click);
            // 
            // funcionáriosToolStripMenuItem
            // 
            this.funcionáriosToolStripMenuItem.Name = "funcionáriosToolStripMenuItem";
            this.funcionáriosToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.funcionáriosToolStripMenuItem.Text = "Funcionários";
            this.funcionáriosToolStripMenuItem.Click += new System.EventHandler(this.funcionáriosToolStripMenuItem_Click);
            // 
            // veículosToolStripMenuItem
            // 
            this.veículosToolStripMenuItem.Name = "veículosToolStripMenuItem";
            this.veículosToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.veículosToolStripMenuItem.Text = "Veículos";
            this.veículosToolStripMenuItem.Click += new System.EventHandler(this.veículosToolStripMenuItem_Click);
            // 
            // produtosToolStripMenuItem
            // 
            this.produtosToolStripMenuItem.Name = "produtosToolStripMenuItem";
            this.produtosToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.produtosToolStripMenuItem.Text = "Produtos";
            // 
            // serviçosToolStripMenuItem
            // 
            this.serviçosToolStripMenuItem.Name = "serviçosToolStripMenuItem";
            this.serviçosToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.serviçosToolStripMenuItem.Text = "Serviços ";
            // 
            // marcasEModelosToolStripMenuItem
            // 
            this.marcasEModelosToolStripMenuItem.Name = "marcasEModelosToolStripMenuItem";
            this.marcasEModelosToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.marcasEModelosToolStripMenuItem.Text = "Marcas e Modelos";
            // 
            // unidadesToolStripMenuItem
            // 
            this.unidadesToolStripMenuItem.Name = "unidadesToolStripMenuItem";
            this.unidadesToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.unidadesToolStripMenuItem.Text = "Unidades";
            // 
            // logoffDeToolStripMenuItem
            // 
            this.logoffDeToolStripMenuItem.Name = "logoffDeToolStripMenuItem";
            this.logoffDeToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.logoffDeToolStripMenuItem.Text = "Logoff de:";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.sairToolStripMenuItem.Text = "Sair";
            // 
            // movimentoToolStripMenuItem
            // 
            this.movimentoToolStripMenuItem.Name = "movimentoToolStripMenuItem";
            this.movimentoToolStripMenuItem.Size = new System.Drawing.Size(97, 24);
            this.movimentoToolStripMenuItem.Text = "Movimento";
            // 
            // relatóriosToolStripMenuItem
            // 
            this.relatóriosToolStripMenuItem.Name = "relatóriosToolStripMenuItem";
            this.relatóriosToolStripMenuItem.Size = new System.Drawing.Size(88, 24);
            this.relatóriosToolStripMenuItem.Text = "Relatórios";
            // 
            // utilitáriosToolStripMenuItem
            // 
            this.utilitáriosToolStripMenuItem.Name = "utilitáriosToolStripMenuItem";
            this.utilitáriosToolStripMenuItem.Size = new System.Drawing.Size(85, 24);
            this.utilitáriosToolStripMenuItem.Text = "Utilitários";
            // 
            // ajudaToolStripMenuItem
            // 
            this.ajudaToolStripMenuItem.Name = "ajudaToolStripMenuItem";
            this.ajudaToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.ajudaToolStripMenuItem.Text = "Ajuda";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.BTNsair);
            this.panel1.Controls.Add(this.BTNcalendario);
            this.panel1.Controls.Add(this.BTNcalculadora);
            this.panel1.Controls.Add(this.BTNordemServico);
            this.panel1.Controls.Add(this.BTNorcamento);
            this.panel1.Controls.Add(this.BTNprodutos);
            this.panel1.Controls.Add(this.BTNveiculos);
            this.panel1.Controls.Add(this.BTNfuncionarios);
            this.panel1.Controls.Add(this.BTNfornecedores);
            this.panel1.Controls.Add(this.BTNclientes);
            this.panel1.Location = new System.Drawing.Point(0, 33);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1265, 57);
            this.panel1.TabIndex = 1;
            // 
            // BTNsair
            // 
            this.BTNsair.Location = new System.Drawing.Point(996, 15);
            this.BTNsair.Margin = new System.Windows.Forms.Padding(4);
            this.BTNsair.Name = "BTNsair";
            this.BTNsair.Size = new System.Drawing.Size(100, 38);
            this.BTNsair.TabIndex = 9;
            this.BTNsair.Text = "Sair";
            this.BTNsair.UseVisualStyleBackColor = true;
            // 
            // BTNcalendario
            // 
            this.BTNcalendario.Location = new System.Drawing.Point(888, 15);
            this.BTNcalendario.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcalendario.Name = "BTNcalendario";
            this.BTNcalendario.Size = new System.Drawing.Size(100, 38);
            this.BTNcalendario.TabIndex = 8;
            this.BTNcalendario.Text = "Calendario";
            this.BTNcalendario.UseVisualStyleBackColor = true;
            // 
            // BTNcalculadora
            // 
            this.BTNcalculadora.Location = new System.Drawing.Point(780, 15);
            this.BTNcalculadora.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcalculadora.Name = "BTNcalculadora";
            this.BTNcalculadora.Size = new System.Drawing.Size(100, 38);
            this.BTNcalculadora.TabIndex = 7;
            this.BTNcalculadora.Text = "Calculadora";
            this.BTNcalculadora.UseVisualStyleBackColor = true;
            this.BTNcalculadora.Click += new System.EventHandler(this.BTNcalculadora_Click);
            // 
            // BTNordemServico
            // 
            this.BTNordemServico.Location = new System.Drawing.Point(659, 15);
            this.BTNordemServico.Margin = new System.Windows.Forms.Padding(4);
            this.BTNordemServico.Name = "BTNordemServico";
            this.BTNordemServico.Size = new System.Drawing.Size(113, 38);
            this.BTNordemServico.TabIndex = 6;
            this.BTNordemServico.Text = "Ordem Servico";
            this.BTNordemServico.UseVisualStyleBackColor = true;
            // 
            // BTNorcamento
            // 
            this.BTNorcamento.Location = new System.Drawing.Point(551, 15);
            this.BTNorcamento.Margin = new System.Windows.Forms.Padding(4);
            this.BTNorcamento.Name = "BTNorcamento";
            this.BTNorcamento.Size = new System.Drawing.Size(100, 38);
            this.BTNorcamento.TabIndex = 5;
            this.BTNorcamento.Text = "Orçamento";
            this.BTNorcamento.UseVisualStyleBackColor = true;
            // 
            // BTNprodutos
            // 
            this.BTNprodutos.Location = new System.Drawing.Point(443, 15);
            this.BTNprodutos.Margin = new System.Windows.Forms.Padding(4);
            this.BTNprodutos.Name = "BTNprodutos";
            this.BTNprodutos.Size = new System.Drawing.Size(100, 38);
            this.BTNprodutos.TabIndex = 4;
            this.BTNprodutos.Text = "Produtos";
            this.BTNprodutos.UseVisualStyleBackColor = true;
            // 
            // BTNveiculos
            // 
            this.BTNveiculos.Location = new System.Drawing.Point(335, 15);
            this.BTNveiculos.Margin = new System.Windows.Forms.Padding(4);
            this.BTNveiculos.Name = "BTNveiculos";
            this.BTNveiculos.Size = new System.Drawing.Size(100, 38);
            this.BTNveiculos.TabIndex = 3;
            this.BTNveiculos.Text = "Veículos";
            this.BTNveiculos.UseVisualStyleBackColor = true;
            this.BTNveiculos.Click += new System.EventHandler(this.BTNveiculos_Click);
            // 
            // BTNfuncionarios
            // 
            this.BTNfuncionarios.Location = new System.Drawing.Point(227, 15);
            this.BTNfuncionarios.Margin = new System.Windows.Forms.Padding(4);
            this.BTNfuncionarios.Name = "BTNfuncionarios";
            this.BTNfuncionarios.Size = new System.Drawing.Size(100, 38);
            this.BTNfuncionarios.TabIndex = 2;
            this.BTNfuncionarios.Text = "Funcionários";
            this.BTNfuncionarios.UseVisualStyleBackColor = true;
            // 
            // BTNfornecedores
            // 
            this.BTNfornecedores.Location = new System.Drawing.Point(112, 15);
            this.BTNfornecedores.Margin = new System.Windows.Forms.Padding(4);
            this.BTNfornecedores.Name = "BTNfornecedores";
            this.BTNfornecedores.Size = new System.Drawing.Size(107, 38);
            this.BTNfornecedores.TabIndex = 1;
            this.BTNfornecedores.Text = "Fornecedores";
            this.BTNfornecedores.UseVisualStyleBackColor = true;
            // 
            // BTNclientes
            // 
            this.BTNclientes.Location = new System.Drawing.Point(4, 15);
            this.BTNclientes.Margin = new System.Windows.Forms.Padding(4);
            this.BTNclientes.Name = "BTNclientes";
            this.BTNclientes.Size = new System.Drawing.Size(100, 38);
            this.BTNclientes.TabIndex = 0;
            this.BTNclientes.Text = "Clientes";
            this.BTNclientes.UseVisualStyleBackColor = true;
            this.BTNclientes.Click += new System.EventHandler(this.BTNclientes_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.TSPdata,
            this.TSPnmUsuario});
            this.statusStrip1.Location = new System.Drawing.Point(0, 722);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1265, 25);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(156, 20);
            this.toolStripStatusLabel1.Text = "Bem Vindo ao SIGMA!";
            // 
            // TSPdata
            // 
            this.TSPdata.Name = "TSPdata";
            this.TSPdata.Size = new System.Drawing.Size(46, 20);
            this.TSPdata.Text = "DATA";
            // 
            // TSPnmUsuario
            // 
            this.TSPnmUsuario.Name = "TSPnmUsuario";
            this.TSPnmUsuario.Size = new System.Drawing.Size(62, 20);
            this.TSPnmUsuario.Text = "Usuário:";
            // 
            // panelCentrofrmSIGMA
            // 
            this.panelCentrofrmSIGMA.BackgroundImage = global::SIGMA.Properties.Resources._6857466_grid_game;
            this.panelCentrofrmSIGMA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelCentrofrmSIGMA.Location = new System.Drawing.Point(0, 94);
            this.panelCentrofrmSIGMA.Margin = new System.Windows.Forms.Padding(4);
            this.panelCentrofrmSIGMA.Name = "panelCentrofrmSIGMA";
            this.panelCentrofrmSIGMA.Size = new System.Drawing.Size(1265, 623);
            this.panelCentrofrmSIGMA.TabIndex = 3;
            // 
            // FRMsigma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1265, 747);
            this.Controls.Add(this.panelCentrofrmSIGMA);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRMsigma";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRMsigma";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fornecedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funcionáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem veículosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviçosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marcasEModelosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoffDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem movimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relatóriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utilitáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajudaToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BTNsair;
        private System.Windows.Forms.Button BTNcalendario;
        private System.Windows.Forms.Button BTNcalculadora;
        private System.Windows.Forms.Button BTNordemServico;
        private System.Windows.Forms.Button BTNorcamento;
        private System.Windows.Forms.Button BTNprodutos;
        private System.Windows.Forms.Button BTNveiculos;
        private System.Windows.Forms.Button BTNfuncionarios;
        private System.Windows.Forms.Button BTNfornecedores;
        private System.Windows.Forms.Button BTNclientes;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel TSPdata;
        private System.Windows.Forms.ToolStripStatusLabel TSPnmUsuario;
        private System.Windows.Forms.Panel panelCentrofrmSIGMA;
    }
}