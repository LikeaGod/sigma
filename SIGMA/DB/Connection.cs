﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.DB
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string ConnectionString = "server=localhost;database=sigmadb2;uid=root;password=1234;sslmode=none";

            MySqlConnection connection = new MySqlConnection(ConnectionString);
            connection.Open();

            return connection;
        }
    }
}
