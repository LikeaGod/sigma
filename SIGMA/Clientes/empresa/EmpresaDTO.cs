﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Clientes.empresa
{
    class EmpresaDTO
    {
        public int Id_Empresa{ get; set; }

        public String Razao_Social { get; set; }

        public String CNPJ { get; set; }

        public String Atividade { get; set; }

        public DateTime Data_Fundacao { get; set; }

        public DateTime Inicio { get; set; }

        public String Rua { get; set; }

        public String Bairro { get; set; }

        public String CEP { get; set; }

        public String Numero { get; set; }

        public String Cidade { get; set; }

        public String Referencia { get; set; }

        public String Complemento { get; set; }

        public String Estado { get; set; }

        public String Telefone { get; set; }

        public String Telefone_Comercial { get; set; }

        public String Celular { get; set; }

        public String Email { get; set; }

        public String Ramal { get; set; }
    }
}
