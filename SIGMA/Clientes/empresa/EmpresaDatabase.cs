﻿using MySql.Data.MySqlClient;
using SIGMA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Clientes.empresa
{
    class EmpresaDatabase
    {
        public int Salvar(EmpresaDTO dto)
        {
            string script = "INSERT INTO tb_empresa(ds_razao_social, ds_cnpj, ds_atividade, dt_data_fund, dt_inic, ds_rua, ds_bairro, ds_cidade, ds_numero, ds_cep, ds_referencia, ds_complemento, ds_estado, ds_telefone, ds_telefone_comercial, ds_celular, ds_email, ds_ramal) VALUES(@ds_razao_social, @ds_cnpj, @ds_atividade, @dt_data_fund, @dt_inic, @ds_rua, @ds_bairro, @ds_cidade, @ds_numero, @ds_cep, @ds_referencia, @ds_complemento, @ds_estado, @ds_telefone, @ds_telefone_comercial, @ds_celular, @ds_email, @ds_ramal)";


            List<MySqlParameter> parms = new List<MySqlParameter>
            {
             new MySqlParameter("ds_razao_social",dto.Razao_Social),
             new MySqlParameter("ds_cnpj",dto.CNPJ),
             new MySqlParameter("ds_atividade",dto.Atividade),
             new MySqlParameter("dt_data_fund",dto.Data_Fundacao),
             new MySqlParameter("dt_inic",dto.Inicio),
             new MySqlParameter("ds_rua",dto.Rua),
             new MySqlParameter("ds_bairro",dto.Bairro),
             new MySqlParameter("ds_cidade",dto.Cidade),
             new MySqlParameter("ds_numero",dto.Numero),
             new MySqlParameter("ds_cep",dto.CEP),
             new MySqlParameter("ds_referencia",dto.Referencia),
             new MySqlParameter("ds_complemento",dto.Complemento),
             new MySqlParameter("ds_estado",dto.Estado),
             new MySqlParameter("ds_telefone",dto.Telefone),
             new MySqlParameter("ds_telefone_comercial",dto.Telefone_Comercial),
             new MySqlParameter("ds_celular",dto.Celular),
             new MySqlParameter("ds_email",dto.Email),
             new MySqlParameter("ds_ramal",dto.Ramal)
            };
            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script ,parms);

            return id;

        }



    }
}
