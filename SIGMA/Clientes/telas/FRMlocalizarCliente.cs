﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA.Clientes.telas
{
    public partial class FRMlocalizarCliente : UserControl
    {
        BindingList<DTOcliente> dtolist = new BindingList<DTOcliente>();
        public FRMlocalizarCliente()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                dgvBuscarCliente.AutoGenerateColumns = false;
            DTOcliente dto = new DTOcliente();
            BusinessCliente bus = new BusinessCliente();
            if (rdnCodigo.Checked == true)
            {
                dto.ID = Convert.ToInt32(txtValordeBusca.Text);
                bus.ConsultarID(dto);
            }
            else if (rdnDescricao.Checked == true)
            {
                dto.Nome = txtValordeBusca.Text;
                bus.ConsultarNome(dto);
            }
                if (dto.Nome != string.Empty)
                {
                    dtolist.Add(dto);
                }
            
            dgvBuscarCliente.DataSource = dtolist;

        }
            catch (Exception erro)
            {
                MessageBox.Show(erro.Message);

            }
}
    }
}
