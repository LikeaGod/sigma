﻿namespace SIGMA.Clientes.telas
{
    partial class FRMpessoaJuridica
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelpessoaJuridica = new System.Windows.Forms.Panel();
            this.mtbTelefonePJ = new System.Windows.Forms.MaskedTextBox();
            this.mtbCelularPJ = new System.Windows.Forms.MaskedTextBox();
            this.mtbTelefComercialPJ = new System.Windows.Forms.MaskedTextBox();
            this.txtRamalPJ = new System.Windows.Forms.TextBox();
            this.txtEmailPJ = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cboEstadoPJ = new System.Windows.Forms.ComboBox();
            this.mtbNumeroPJ = new System.Windows.Forms.MaskedTextBox();
            this.MTBcepPJ = new System.Windows.Forms.MaskedTextBox();
            this.txtComplementoPJ = new System.Windows.Forms.TextBox();
            this.txtRuaPJ = new System.Windows.Forms.TextBox();
            this.txtBairroPJ = new System.Windows.Forms.TextBox();
            this.txtCidadePJ = new System.Windows.Forms.TextBox();
            this.txtReferenciaPJ = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mtbCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.datadeFUNDAÇÃOPJ = new System.Windows.Forms.DateTimePicker();
            this.txtRazaoSocialPJ = new System.Windows.Forms.TextBox();
            this.txtAtividadePJ = new System.Windows.Forms.TextBox();
            this.rdnPessoaJuridicaPJ = new System.Windows.Forms.RadioButton();
            this.rdnPessoaFisicaPJ = new System.Windows.Forms.RadioButton();
            this.LBLclieteDesdePJ = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LBLcodClientePJ = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelpessoaJuridica.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelpessoaJuridica
            // 
            this.panelpessoaJuridica.BackColor = System.Drawing.Color.FloralWhite;
            this.panelpessoaJuridica.Controls.Add(this.mtbTelefonePJ);
            this.panelpessoaJuridica.Controls.Add(this.mtbCelularPJ);
            this.panelpessoaJuridica.Controls.Add(this.mtbTelefComercialPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtRamalPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtEmailPJ);
            this.panelpessoaJuridica.Controls.Add(this.label22);
            this.panelpessoaJuridica.Controls.Add(this.label21);
            this.panelpessoaJuridica.Controls.Add(this.label20);
            this.panelpessoaJuridica.Controls.Add(this.label19);
            this.panelpessoaJuridica.Controls.Add(this.label18);
            this.panelpessoaJuridica.Controls.Add(this.label17);
            this.panelpessoaJuridica.Controls.Add(this.cboEstadoPJ);
            this.panelpessoaJuridica.Controls.Add(this.mtbNumeroPJ);
            this.panelpessoaJuridica.Controls.Add(this.MTBcepPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtComplementoPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtRuaPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtBairroPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtCidadePJ);
            this.panelpessoaJuridica.Controls.Add(this.txtReferenciaPJ);
            this.panelpessoaJuridica.Controls.Add(this.label16);
            this.panelpessoaJuridica.Controls.Add(this.label15);
            this.panelpessoaJuridica.Controls.Add(this.label14);
            this.panelpessoaJuridica.Controls.Add(this.label13);
            this.panelpessoaJuridica.Controls.Add(this.label12);
            this.panelpessoaJuridica.Controls.Add(this.label11);
            this.panelpessoaJuridica.Controls.Add(this.label10);
            this.panelpessoaJuridica.Controls.Add(this.label9);
            this.panelpessoaJuridica.Controls.Add(this.label4);
            this.panelpessoaJuridica.Controls.Add(this.mtbCNPJ);
            this.panelpessoaJuridica.Controls.Add(this.datadeFUNDAÇÃOPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtRazaoSocialPJ);
            this.panelpessoaJuridica.Controls.Add(this.txtAtividadePJ);
            this.panelpessoaJuridica.Controls.Add(this.rdnPessoaJuridicaPJ);
            this.panelpessoaJuridica.Controls.Add(this.rdnPessoaFisicaPJ);
            this.panelpessoaJuridica.Controls.Add(this.LBLclieteDesdePJ);
            this.panelpessoaJuridica.Controls.Add(this.label8);
            this.panelpessoaJuridica.Controls.Add(this.label7);
            this.panelpessoaJuridica.Controls.Add(this.label6);
            this.panelpessoaJuridica.Controls.Add(this.label5);
            this.panelpessoaJuridica.Controls.Add(this.LBLcodClientePJ);
            this.panelpessoaJuridica.Controls.Add(this.label3);
            this.panelpessoaJuridica.Controls.Add(this.label2);
            this.panelpessoaJuridica.Controls.Add(this.label1);
            this.panelpessoaJuridica.Location = new System.Drawing.Point(0, 0);
            this.panelpessoaJuridica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelpessoaJuridica.Name = "panelpessoaJuridica";
            this.panelpessoaJuridica.Size = new System.Drawing.Size(1184, 569);
            this.panelpessoaJuridica.TabIndex = 0;
            // 
            // mtbTelefonePJ
            // 
            this.mtbTelefonePJ.Location = new System.Drawing.Point(184, 450);
            this.mtbTelefonePJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbTelefonePJ.Mask = "(00)0000-0000";
            this.mtbTelefonePJ.Name = "mtbTelefonePJ";
            this.mtbTelefonePJ.Size = new System.Drawing.Size(235, 22);
            this.mtbTelefonePJ.TabIndex = 43;
            // 
            // mtbCelularPJ
            // 
            this.mtbCelularPJ.Location = new System.Drawing.Point(184, 487);
            this.mtbCelularPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbCelularPJ.Mask = "(00)0000-0000";
            this.mtbCelularPJ.Name = "mtbCelularPJ";
            this.mtbCelularPJ.Size = new System.Drawing.Size(235, 22);
            this.mtbCelularPJ.TabIndex = 42;
            // 
            // mtbTelefComercialPJ
            // 
            this.mtbTelefComercialPJ.Location = new System.Drawing.Point(701, 449);
            this.mtbTelefComercialPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbTelefComercialPJ.Mask = "(00)0000-0000";
            this.mtbTelefComercialPJ.Name = "mtbTelefComercialPJ";
            this.mtbTelefComercialPJ.Size = new System.Drawing.Size(120, 22);
            this.mtbTelefComercialPJ.TabIndex = 41;
            // 
            // txtRamalPJ
            // 
            this.txtRamalPJ.Location = new System.Drawing.Point(1003, 449);
            this.txtRamalPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRamalPJ.Name = "txtRamalPJ";
            this.txtRamalPJ.Size = new System.Drawing.Size(151, 22);
            this.txtRamalPJ.TabIndex = 40;
            // 
            // txtEmailPJ
            // 
            this.txtEmailPJ.Location = new System.Drawing.Point(701, 487);
            this.txtEmailPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmailPJ.Name = "txtEmailPJ";
            this.txtEmailPJ.Size = new System.Drawing.Size(235, 22);
            this.txtEmailPJ.TabIndex = 39;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label22.Location = new System.Drawing.Point(7, 393);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 29);
            this.label22.TabIndex = 38;
            this.label22.Text = "Contato";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(60, 478);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(116, 31);
            this.label21.TabIndex = 37;
            this.label21.Text = "Celular:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(39, 441);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(137, 31);
            this.label20.TabIndex = 36;
            this.label20.Text = "Telefone:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(889, 442);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(106, 31);
            this.label19.TabIndex = 35;
            this.label19.Text = "Ramal:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(606, 478);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 31);
            this.label18.TabIndex = 34;
            this.label18.Text = "Email:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(427, 441);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(276, 31);
            this.label17.TabIndex = 33;
            this.label17.Text = "Telefone Comercial:";
            // 
            // cboEstadoPJ
            // 
            this.cboEstadoPJ.FormattingEnabled = true;
            this.cboEstadoPJ.Location = new System.Drawing.Point(968, 298);
            this.cboEstadoPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboEstadoPJ.Name = "cboEstadoPJ";
            this.cboEstadoPJ.Size = new System.Drawing.Size(195, 24);
            this.cboEstadoPJ.TabIndex = 32;
            // 
            // mtbNumeroPJ
            // 
            this.mtbNumeroPJ.Location = new System.Drawing.Point(609, 262);
            this.mtbNumeroPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbNumeroPJ.Name = "mtbNumeroPJ";
            this.mtbNumeroPJ.Size = new System.Drawing.Size(145, 22);
            this.mtbNumeroPJ.TabIndex = 31;
            // 
            // MTBcepPJ
            // 
            this.MTBcepPJ.Location = new System.Drawing.Point(609, 295);
            this.MTBcepPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MTBcepPJ.Name = "MTBcepPJ";
            this.MTBcepPJ.Size = new System.Drawing.Size(145, 22);
            this.MTBcepPJ.TabIndex = 30;
            // 
            // txtComplementoPJ
            // 
            this.txtComplementoPJ.Location = new System.Drawing.Point(967, 265);
            this.txtComplementoPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtComplementoPJ.Name = "txtComplementoPJ";
            this.txtComplementoPJ.Size = new System.Drawing.Size(195, 22);
            this.txtComplementoPJ.TabIndex = 28;
            // 
            // txtRuaPJ
            // 
            this.txtRuaPJ.Location = new System.Drawing.Point(184, 265);
            this.txtRuaPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRuaPJ.Name = "txtRuaPJ";
            this.txtRuaPJ.Size = new System.Drawing.Size(235, 22);
            this.txtRuaPJ.TabIndex = 27;
            // 
            // txtBairroPJ
            // 
            this.txtBairroPJ.Location = new System.Drawing.Point(184, 295);
            this.txtBairroPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBairroPJ.Name = "txtBairroPJ";
            this.txtBairroPJ.Size = new System.Drawing.Size(235, 22);
            this.txtBairroPJ.TabIndex = 26;
            // 
            // txtCidadePJ
            // 
            this.txtCidadePJ.Location = new System.Drawing.Point(184, 331);
            this.txtCidadePJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCidadePJ.Name = "txtCidadePJ";
            this.txtCidadePJ.Size = new System.Drawing.Size(235, 22);
            this.txtCidadePJ.TabIndex = 25;
            // 
            // txtReferenciaPJ
            // 
            this.txtReferenciaPJ.Location = new System.Drawing.Point(609, 331);
            this.txtReferenciaPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtReferenciaPJ.Name = "txtReferenciaPJ";
            this.txtReferenciaPJ.Size = new System.Drawing.Size(235, 22);
            this.txtReferenciaPJ.TabIndex = 24;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(437, 322);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(165, 31);
            this.label16.TabIndex = 23;
            this.label16.Text = "Referência:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(519, 285);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 31);
            this.label15.TabIndex = 22;
            this.label15.Text = "CEP:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(762, 256);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(202, 31);
            this.label14.TabIndex = 21;
            this.label14.Text = "Complemento:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label13.Location = new System.Drawing.Point(477, 253);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 31);
            this.label13.TabIndex = 20;
            this.label13.Text = "Número:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(850, 289);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 31);
            this.label12.TabIndex = 19;
            this.label12.Text = "Estado:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(85, 287);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 31);
            this.label11.TabIndex = 18;
            this.label11.Text = "Bairro:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(72, 322);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 31);
            this.label10.TabIndex = 17;
            this.label10.Text = "Cidade:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(112, 256);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 31);
            this.label9.TabIndex = 16;
            this.label9.Text = "Rua:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label4.Location = new System.Drawing.Point(4, 224);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 29);
            this.label4.TabIndex = 15;
            this.label4.Text = "Endereço";
            // 
            // mtbCNPJ
            // 
            this.mtbCNPJ.Location = new System.Drawing.Point(219, 135);
            this.mtbCNPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbCNPJ.Mask = "00.000.000/0000-00";
            this.mtbCNPJ.Name = "mtbCNPJ";
            this.mtbCNPJ.Size = new System.Drawing.Size(235, 22);
            this.mtbCNPJ.TabIndex = 14;
            // 
            // datadeFUNDAÇÃOPJ
            // 
            this.datadeFUNDAÇÃOPJ.Location = new System.Drawing.Point(810, 126);
            this.datadeFUNDAÇÃOPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.datadeFUNDAÇÃOPJ.Name = "datadeFUNDAÇÃOPJ";
            this.datadeFUNDAÇÃOPJ.Size = new System.Drawing.Size(283, 22);
            this.datadeFUNDAÇÃOPJ.TabIndex = 13;
            // 
            // txtRazaoSocialPJ
            // 
            this.txtRazaoSocialPJ.Location = new System.Drawing.Point(219, 96);
            this.txtRazaoSocialPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRazaoSocialPJ.Name = "txtRazaoSocialPJ";
            this.txtRazaoSocialPJ.Size = new System.Drawing.Size(235, 22);
            this.txtRazaoSocialPJ.TabIndex = 12;
            // 
            // txtAtividadePJ
            // 
            this.txtAtividadePJ.Location = new System.Drawing.Point(675, 91);
            this.txtAtividadePJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAtividadePJ.Name = "txtAtividadePJ";
            this.txtAtividadePJ.Size = new System.Drawing.Size(235, 22);
            this.txtAtividadePJ.TabIndex = 11;
            // 
            // rdnPessoaJuridicaPJ
            // 
            this.rdnPessoaJuridicaPJ.AutoSize = true;
            this.rdnPessoaJuridicaPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPessoaJuridicaPJ.Location = new System.Drawing.Point(1012, 9);
            this.rdnPessoaJuridicaPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdnPessoaJuridicaPJ.Name = "rdnPessoaJuridicaPJ";
            this.rdnPessoaJuridicaPJ.Size = new System.Drawing.Size(150, 24);
            this.rdnPessoaJuridicaPJ.TabIndex = 10;
            this.rdnPessoaJuridicaPJ.TabStop = true;
            this.rdnPessoaJuridicaPJ.Text = "Pessoa Juridica";
            this.rdnPessoaJuridicaPJ.UseVisualStyleBackColor = true;
            // 
            // rdnPessoaFisicaPJ
            // 
            this.rdnPessoaFisicaPJ.AutoSize = true;
            this.rdnPessoaFisicaPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnPessoaFisicaPJ.Location = new System.Drawing.Point(853, 10);
            this.rdnPessoaFisicaPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdnPessoaFisicaPJ.Name = "rdnPessoaFisicaPJ";
            this.rdnPessoaFisicaPJ.Size = new System.Drawing.Size(136, 24);
            this.rdnPessoaFisicaPJ.TabIndex = 9;
            this.rdnPessoaFisicaPJ.TabStop = true;
            this.rdnPessoaFisicaPJ.Text = "Pessoa Física";
            this.rdnPessoaFisicaPJ.UseVisualStyleBackColor = true;
            // 
            // LBLclieteDesdePJ
            // 
            this.LBLclieteDesdePJ.AutoSize = true;
            this.LBLclieteDesdePJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.LBLclieteDesdePJ.Location = new System.Drawing.Point(1005, 165);
            this.LBLclieteDesdePJ.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLclieteDesdePJ.Name = "LBLclieteDesdePJ";
            this.LBLclieteDesdePJ.Size = new System.Drawing.Size(128, 31);
            this.LBLclieteDesdePJ.TabIndex = 8;
            this.LBLclieteDesdePJ.Text = "21/15/12";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(549, 82);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 31);
            this.label8.TabIndex = 7;
            this.label8.Text = "Atividade:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(549, 119);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(253, 31);
            this.label7.TabIndex = 6;
            this.label7.Text = "Data de Fundação";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(847, 165);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(202, 31);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cliente desde:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(112, 126);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 31);
            this.label5.TabIndex = 4;
            this.label5.Text = "CNPJ:";
            // 
            // LBLcodClientePJ
            // 
            this.LBLcodClientePJ.AutoSize = true;
            this.LBLcodClientePJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.LBLcodClientePJ.Location = new System.Drawing.Point(188, 52);
            this.LBLcodClientePJ.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLcodClientePJ.Name = "LBLcodClientePJ";
            this.LBLcodClientePJ.Size = new System.Drawing.Size(30, 31);
            this.LBLcodClientePJ.TabIndex = 3;
            this.LBLcodClientePJ.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(16, 87);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "Razão Social:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(92, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "Código:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente";
            // 
            // FRMpessoaJuridica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelpessoaJuridica);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRMpessoaJuridica";
            this.Size = new System.Drawing.Size(1184, 569);
            this.panelpessoaJuridica.ResumeLayout(false);
            this.panelpessoaJuridica.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelpessoaJuridica;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LBLcodClientePJ;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker datadeFUNDAÇÃOPJ;
        private System.Windows.Forms.TextBox txtRazaoSocialPJ;
        private System.Windows.Forms.TextBox txtAtividadePJ;
        private System.Windows.Forms.RadioButton rdnPessoaJuridicaPJ;
        private System.Windows.Forms.RadioButton rdnPessoaFisicaPJ;
        private System.Windows.Forms.Label LBLclieteDesdePJ;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox mtbCNPJ;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboEstadoPJ;
        private System.Windows.Forms.MaskedTextBox mtbNumeroPJ;
        private System.Windows.Forms.MaskedTextBox MTBcepPJ;
        private System.Windows.Forms.TextBox txtComplementoPJ;
        private System.Windows.Forms.TextBox txtRuaPJ;
        private System.Windows.Forms.TextBox txtBairroPJ;
        private System.Windows.Forms.TextBox txtCidadePJ;
        private System.Windows.Forms.TextBox txtReferenciaPJ;
        private System.Windows.Forms.MaskedTextBox mtbTelefonePJ;
        private System.Windows.Forms.MaskedTextBox mtbCelularPJ;
        private System.Windows.Forms.MaskedTextBox mtbTelefComercialPJ;
        private System.Windows.Forms.TextBox txtRamalPJ;
        private System.Windows.Forms.TextBox txtEmailPJ;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
    }
}
