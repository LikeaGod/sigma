﻿namespace SIGMA.Clientes.telas
{
    partial class FRMlocalizarCliente
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLocalizarCliente = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCANCELAR = new System.Windows.Forms.Button();
            this.dgvBuscarCliente = new System.Windows.Forms.DataGridView();
            this.txtValordeBusca = new System.Windows.Forms.TextBox();
            this.rdnDescricao = new System.Windows.Forms.RadioButton();
            this.rdnCodigo = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelLocalizarCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBuscarCliente)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLocalizarCliente
            // 
            this.panelLocalizarCliente.BackColor = System.Drawing.Color.LemonChiffon;
            this.panelLocalizarCliente.Controls.Add(this.btnOK);
            this.panelLocalizarCliente.Controls.Add(this.btnCANCELAR);
            this.panelLocalizarCliente.Controls.Add(this.dgvBuscarCliente);
            this.panelLocalizarCliente.Controls.Add(this.txtValordeBusca);
            this.panelLocalizarCliente.Controls.Add(this.rdnDescricao);
            this.panelLocalizarCliente.Controls.Add(this.rdnCodigo);
            this.panelLocalizarCliente.Controls.Add(this.label3);
            this.panelLocalizarCliente.Controls.Add(this.label2);
            this.panelLocalizarCliente.Controls.Add(this.label1);
            this.panelLocalizarCliente.Location = new System.Drawing.Point(0, 0);
            this.panelLocalizarCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelLocalizarCliente.Name = "panelLocalizarCliente";
            this.panelLocalizarCliente.Size = new System.Drawing.Size(877, 517);
            this.panelLocalizarCliente.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(536, 459);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(151, 43);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCANCELAR
            // 
            this.btnCANCELAR.Location = new System.Drawing.Point(707, 459);
            this.btnCANCELAR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCANCELAR.Name = "btnCANCELAR";
            this.btnCANCELAR.Size = new System.Drawing.Size(151, 43);
            this.btnCANCELAR.TabIndex = 7;
            this.btnCANCELAR.Text = "CANCELAR";
            this.btnCANCELAR.UseVisualStyleBackColor = true;
            // 
            // dgvBuscarCliente
            // 
            this.dgvBuscarCliente.AllowUserToAddRows = false;
            this.dgvBuscarCliente.AllowUserToDeleteRows = false;
            this.dgvBuscarCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBuscarCliente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.nomeCliente});
            this.dgvBuscarCliente.Location = new System.Drawing.Point(11, 167);
            this.dgvBuscarCliente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvBuscarCliente.Name = "dgvBuscarCliente";
            this.dgvBuscarCliente.ReadOnly = true;
            this.dgvBuscarCliente.RowHeadersVisible = false;
            this.dgvBuscarCliente.Size = new System.Drawing.Size(847, 258);
            this.dgvBuscarCliente.TabIndex = 6;
            // 
            // txtValordeBusca
            // 
            this.txtValordeBusca.Location = new System.Drawing.Point(258, 95);
            this.txtValordeBusca.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtValordeBusca.Multiline = true;
            this.txtValordeBusca.Name = "txtValordeBusca";
            this.txtValordeBusca.Size = new System.Drawing.Size(511, 31);
            this.txtValordeBusca.TabIndex = 5;
            // 
            // rdnDescricao
            // 
            this.rdnDescricao.AutoSize = true;
            this.rdnDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnDescricao.Location = new System.Drawing.Point(11, 123);
            this.rdnDescricao.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdnDescricao.Name = "rdnDescricao";
            this.rdnDescricao.Size = new System.Drawing.Size(107, 24);
            this.rdnDescricao.TabIndex = 4;
            this.rdnDescricao.TabStop = true;
            this.rdnDescricao.Text = "Descrição";
            this.rdnDescricao.UseVisualStyleBackColor = true;
            // 
            // rdnCodigo
            // 
            this.rdnCodigo.AutoSize = true;
            this.rdnCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnCodigo.Location = new System.Drawing.Point(9, 95);
            this.rdnCodigo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdnCodigo.Name = "rdnCodigo";
            this.rdnCodigo.Size = new System.Drawing.Size(82, 24);
            this.rdnCodigo.TabIndex = 3;
            this.rdnCodigo.TabStop = true;
            this.rdnCodigo.Text = "Código";
            this.rdnCodigo.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(4, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tipo de Busca";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(252, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(606, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Escolha o Tipo de Busca, digite o valor e aperte ENTER";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Localizar Cliente";
            // 
            // Codigo
            // 
            this.Codigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Codigo.DataPropertyName = "ID";
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            // 
            // nomeCliente
            // 
            this.nomeCliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nomeCliente.DataPropertyName = "Nome";
            this.nomeCliente.HeaderText = "Nome do cliente";
            this.nomeCliente.Name = "nomeCliente";
            this.nomeCliente.ReadOnly = true;
            // 
            // FRMlocalizarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelLocalizarCliente);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FRMlocalizarCliente";
            this.Size = new System.Drawing.Size(877, 521);
            this.panelLocalizarCliente.ResumeLayout(false);
            this.panelLocalizarCliente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBuscarCliente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLocalizarCliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCANCELAR;
        private System.Windows.Forms.DataGridView dgvBuscarCliente;
        private System.Windows.Forms.TextBox txtValordeBusca;
        private System.Windows.Forms.RadioButton rdnDescricao;
        private System.Windows.Forms.RadioButton rdnCodigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeCliente;
    }
}
