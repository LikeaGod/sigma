﻿using SIGMA.Clientes.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA.Clientes
{
    public partial class FRMcadastroCliente : Form
    {
        public FRMcadastroCliente()
        {
            InitializeComponent();
            DateTime date = DateTime.Now;
            string dates = date.ToString("dd/mm/yyyy");
            LBLclienteDesdePF.Text = dates;
            CarregarCombos();
        }

        public void CarregarCombos()
        {
            List<string> Estados = new List<string>();
            Estados.Add("Acre");
            Estados.Add("Alagoas");
            Estados.Add("Amapá");
            Estados.Add("Amazonas");
            Estados.Add("Bahia");
            Estados.Add("Ceará");
            Estados.Add("Distrito Federal");
            Estados.Add("Espírito Santo");
            Estados.Add("Goiás");
            Estados.Add("Maranhão");
            Estados.Add("Mato Grosso");
            Estados.Add("Mato Grosso do Sul");
            Estados.Add("Minas Gerais");
            Estados.Add("Pará");
            Estados.Add("Paraná");
            Estados.Add("Pernambuco");
            Estados.Add("Piauí");
            Estados.Add("Rio de Janeiro");
            Estados.Add("Rio Grande do Norte");
            Estados.Add("Rio Grande do Sul");
            Estados.Add("Rondônia");
            Estados.Add("Roraima");
            Estados.Add("Santa Catarina");
            Estados.Add("São Paulo");
            Estados.Add("Sergipe");
            Estados.Add("Tocantins");
            cboEstado.DataSource = Estados;
        }

       

        private void btnClienteSALVAR_Click_1(object sender, EventArgs e)
        {
            DTOcliente dto = new DTOcliente();
            dto.Bairro = txtBairro.Text;
            dto.Celular = txtCelular.Text;
            dto.CEP = txtCep.Text;
            dto.Cidade = txtCidade.Text;
            dto.Complemento = txtComplemento.Text;
            dto.CPF = txtCPF.Text;
            dto.Data = dtpCliente.Value;
            dto.Email = txtEmail.Text;
            dto.Estado = cboEstado.SelectedItem.ToString();
            dto.Inicio = DateTime.Now;
            dto.Nome = txtNomeCliente.Text;
            dto.Numero = txtNumero.Text;
            dto.Profissao = txtProfissaoCliente.Text;
            dto.Ramal = txtRamal.Text;
            dto.Referencia = txtReferencia.Text;
            dto.Rua = txtRua.Text;
            if (rdnFem.Checked == true)
            {
                dto.Sexo = "f";
            }
            else
            {
                dto.Sexo = "m";
            }
            dto.Telefone = txtTel.Text;
            dto.Telefone_Comercial = txtTelComecial.Text;
            BusinessCliente buss = new BusinessCliente();
            int id = buss.Salvar(dto);
        }
    }
}
