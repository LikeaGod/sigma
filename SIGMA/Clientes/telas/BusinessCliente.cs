﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Clientes.telas
{
    class BusinessCliente
    {
        public int Salvar(DTOcliente dto)
        {
            DatabaseCliente dbCliente = new DatabaseCliente();
            int id = dbCliente.Salvar(dto);

            return id;
        }

        public DTOcliente ConsultarID(DTOcliente dto)
        {
            if (dto.ID == 0)
            {
                throw new ArgumentException("Insira o valor de pesquisa valido");
            }
            DatabaseCliente dbCliente = new DatabaseCliente();
            dto = dbCliente.ConsultarClienteID(dto);
            return dto;
        }
        public DTOcliente ConsultarNome(DTOcliente dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Insira o valor de pesquisa valido");
            }
            DatabaseCliente dbCliente = new DatabaseCliente();
            dto = dbCliente.ConsultarClienteNome(dto);
            return dto;
        }
    }
}
