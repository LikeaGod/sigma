﻿using MySql.Data.MySqlClient;
using SIGMA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Clientes.telas
{
    class DatabaseCliente
    {
        public int Salvar(DTOcliente dto)
        {
            string script = "INSERT INTO tb_cliente(nm_cliente, ds_cpf, ds_sexo, ds_profissao, dt_data, dt_inicio, ds_rua, ds_bairro, ds_cep, ds_numero, ds_cidade, ds_referencia, ds_complemento, ds_estado, ds_telefone, ds_telefone_comercial, ds_ramal, ds_celular, ds_email) VALUES(@nm_cliente, @ds_cpf, @ds_sexo, @ds_profissao, @dt_data, @dt_inicio, @ds_rua, @ds_bairro, @ds_cep, @ds_numero, @ds_cidade, @ds_referencia, @ds_complemento, @ds_estado, @ds_telefone, @ds_telefone_comercial, @ds_ramal, @ds_celular, @ds_email)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("ds_profissao", dto.Profissao));
            parms.Add(new MySqlParameter("dt_data", dto.Data));
            parms.Add(new MySqlParameter("dt_inicio", dto.Inicio));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_referencia", dto.Referencia));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_estado", dto.Estado));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_telefone_comercial", dto.Telefone_Comercial));
            parms.Add(new MySqlParameter("ds_ramal", dto.Ramal));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));
            parms.Add(new MySqlParameter("ds_email", dto.Email));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }
        public DTOcliente ConsultarClienteID(DTOcliente dto)
        {
            string script = @"Select * from tb_cliente Where id_cliente like @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ID ));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            while (reader.Read())
            {
                dto.ID = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
            }
            return dto;
        }
        public DTOcliente ConsultarClienteNome(DTOcliente dto)
        {
            string script = @"Select * from tb_cliente Where nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente ", dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            while (reader.Read())
            {
                dto.ID = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
            }
            return dto;
        }
    }
}
