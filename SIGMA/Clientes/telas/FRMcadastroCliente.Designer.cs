﻿namespace SIGMA.Clientes
{
    partial class FRMcadastroCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClienteFECHAR = new System.Windows.Forms.Button();
            this.btnClienteSALVAR = new System.Windows.Forms.Button();
            this.btnClienteExcluir = new System.Windows.Forms.Button();
            this.btnClienteCANCELAR = new System.Windows.Forms.Button();
            this.btnClienteALTERAR = new System.Windows.Forms.Button();
            this.BTNcadClienteINSERIR = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClientePesquisar = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dadosGeraisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.observacoesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelCentrocadCliente = new System.Windows.Forms.Panel();
            this.txtRamal = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTelComecial = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cboEstado = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.LBLclienteDesdePF = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpCliente = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.txtProfissaoCliente = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.txtTel = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.LBLcodCliente = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.rdnFem = new System.Windows.Forms.RadioButton();
            this.rdnMasc = new System.Windows.Forms.RadioButton();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.txtCPF = new System.Windows.Forms.TextBox();
            this.txtNomeCliente = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClientePesquisar)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panelCentrocadCliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.btnClienteFECHAR);
            this.panel1.Controls.Add(this.btnClienteSALVAR);
            this.panel1.Controls.Add(this.btnClienteExcluir);
            this.panel1.Controls.Add(this.btnClienteCANCELAR);
            this.panel1.Controls.Add(this.btnClienteALTERAR);
            this.panel1.Controls.Add(this.BTNcadClienteINSERIR);
            this.panel1.Location = new System.Drawing.Point(-1, 636);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1184, 63);
            this.panel1.TabIndex = 23;
            // 
            // btnClienteFECHAR
            // 
            this.btnClienteFECHAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteFECHAR.Location = new System.Drawing.Point(1048, 9);
            this.btnClienteFECHAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnClienteFECHAR.Name = "btnClienteFECHAR";
            this.btnClienteFECHAR.Size = new System.Drawing.Size(119, 44);
            this.btnClienteFECHAR.TabIndex = 5;
            this.btnClienteFECHAR.Text = "Fechar";
            this.btnClienteFECHAR.UseVisualStyleBackColor = true;
            // 
            // btnClienteSALVAR
            // 
            this.btnClienteSALVAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteSALVAR.Location = new System.Drawing.Point(905, 9);
            this.btnClienteSALVAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnClienteSALVAR.Name = "btnClienteSALVAR";
            this.btnClienteSALVAR.Size = new System.Drawing.Size(119, 44);
            this.btnClienteSALVAR.TabIndex = 4;
            this.btnClienteSALVAR.Text = "Salvar";
            this.btnClienteSALVAR.UseVisualStyleBackColor = true;
            this.btnClienteSALVAR.Click += new System.EventHandler(this.btnClienteSALVAR_Click_1);
            // 
            // btnClienteExcluir
            // 
            this.btnClienteExcluir.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteExcluir.Location = new System.Drawing.Point(456, 4);
            this.btnClienteExcluir.Margin = new System.Windows.Forms.Padding(4);
            this.btnClienteExcluir.Name = "btnClienteExcluir";
            this.btnClienteExcluir.Size = new System.Drawing.Size(119, 44);
            this.btnClienteExcluir.TabIndex = 3;
            this.btnClienteExcluir.Text = "Excluir";
            this.btnClienteExcluir.UseVisualStyleBackColor = true;
            // 
            // btnClienteCANCELAR
            // 
            this.btnClienteCANCELAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteCANCELAR.Location = new System.Drawing.Point(311, 4);
            this.btnClienteCANCELAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnClienteCANCELAR.Name = "btnClienteCANCELAR";
            this.btnClienteCANCELAR.Size = new System.Drawing.Size(119, 44);
            this.btnClienteCANCELAR.TabIndex = 2;
            this.btnClienteCANCELAR.Text = "Cancelar";
            this.btnClienteCANCELAR.UseVisualStyleBackColor = true;
            // 
            // btnClienteALTERAR
            // 
            this.btnClienteALTERAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClienteALTERAR.Location = new System.Drawing.Point(153, 4);
            this.btnClienteALTERAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnClienteALTERAR.Name = "btnClienteALTERAR";
            this.btnClienteALTERAR.Size = new System.Drawing.Size(119, 44);
            this.btnClienteALTERAR.TabIndex = 1;
            this.btnClienteALTERAR.Text = "Alterar";
            this.btnClienteALTERAR.UseVisualStyleBackColor = true;
            // 
            // BTNcadClienteINSERIR
            // 
            this.BTNcadClienteINSERIR.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNcadClienteINSERIR.Location = new System.Drawing.Point(4, 4);
            this.BTNcadClienteINSERIR.Margin = new System.Windows.Forms.Padding(4);
            this.BTNcadClienteINSERIR.Name = "BTNcadClienteINSERIR";
            this.BTNcadClienteINSERIR.Size = new System.Drawing.Size(116, 44);
            this.BTNcadClienteINSERIR.TabIndex = 0;
            this.BTNcadClienteINSERIR.Text = "Inserir";
            this.BTNcadClienteINSERIR.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Controls.Add(this.btnClientePesquisar);
            this.panel2.Controls.Add(this.menuStrip1);
            this.panel2.Location = new System.Drawing.Point(-3, -5);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1185, 96);
            this.panel2.TabIndex = 24;
            // 
            // btnClientePesquisar
            // 
            this.btnClientePesquisar.Location = new System.Drawing.Point(0, 0);
            this.btnClientePesquisar.Name = "btnClientePesquisar";
            this.btnClientePesquisar.Size = new System.Drawing.Size(100, 50);
            this.btnClientePesquisar.TabIndex = 0;
            this.btnClientePesquisar.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dadosGeraisToolStripMenuItem,
            this.observacoesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1185, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dadosGeraisToolStripMenuItem
            // 
            this.dadosGeraisToolStripMenuItem.Name = "dadosGeraisToolStripMenuItem";
            this.dadosGeraisToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.dadosGeraisToolStripMenuItem.Text = "Dados gerais";
            // 
            // observacoesToolStripMenuItem
            // 
            this.observacoesToolStripMenuItem.Name = "observacoesToolStripMenuItem";
            this.observacoesToolStripMenuItem.Size = new System.Drawing.Size(105, 24);
            this.observacoesToolStripMenuItem.Text = "Observações";
            // 
            // panelCentrocadCliente
            // 
            this.panelCentrocadCliente.BackColor = System.Drawing.Color.FloralWhite;
            this.panelCentrocadCliente.Controls.Add(this.txtRamal);
            this.panelCentrocadCliente.Controls.Add(this.label24);
            this.panelCentrocadCliente.Controls.Add(this.txtEmail);
            this.panelCentrocadCliente.Controls.Add(this.label23);
            this.panelCentrocadCliente.Controls.Add(this.txtTelComecial);
            this.panelCentrocadCliente.Controls.Add(this.label22);
            this.panelCentrocadCliente.Controls.Add(this.txtReferencia);
            this.panelCentrocadCliente.Controls.Add(this.label21);
            this.panelCentrocadCliente.Controls.Add(this.cboEstado);
            this.panelCentrocadCliente.Controls.Add(this.label20);
            this.panelCentrocadCliente.Controls.Add(this.txtCep);
            this.panelCentrocadCliente.Controls.Add(this.label19);
            this.panelCentrocadCliente.Controls.Add(this.label18);
            this.panelCentrocadCliente.Controls.Add(this.txtComplemento);
            this.panelCentrocadCliente.Controls.Add(this.txtNumero);
            this.panelCentrocadCliente.Controls.Add(this.label17);
            this.panelCentrocadCliente.Controls.Add(this.LBLclienteDesdePF);
            this.panelCentrocadCliente.Controls.Add(this.label16);
            this.panelCentrocadCliente.Controls.Add(this.dtpCliente);
            this.panelCentrocadCliente.Controls.Add(this.label14);
            this.panelCentrocadCliente.Controls.Add(this.radioButton3);
            this.panelCentrocadCliente.Controls.Add(this.radioButton4);
            this.panelCentrocadCliente.Controls.Add(this.txtProfissaoCliente);
            this.panelCentrocadCliente.Controls.Add(this.label13);
            this.panelCentrocadCliente.Controls.Add(this.txtCelular);
            this.panelCentrocadCliente.Controls.Add(this.txtTel);
            this.panelCentrocadCliente.Controls.Add(this.label11);
            this.panelCentrocadCliente.Controls.Add(this.label12);
            this.panelCentrocadCliente.Controls.Add(this.LBLcodCliente);
            this.panelCentrocadCliente.Controls.Add(this.label10);
            this.panelCentrocadCliente.Controls.Add(this.txtBairro);
            this.panelCentrocadCliente.Controls.Add(this.txtCidade);
            this.panelCentrocadCliente.Controls.Add(this.label9);
            this.panelCentrocadCliente.Controls.Add(this.label8);
            this.panelCentrocadCliente.Controls.Add(this.label7);
            this.panelCentrocadCliente.Controls.Add(this.label6);
            this.panelCentrocadCliente.Controls.Add(this.rdnFem);
            this.panelCentrocadCliente.Controls.Add(this.rdnMasc);
            this.panelCentrocadCliente.Controls.Add(this.txtRua);
            this.panelCentrocadCliente.Controls.Add(this.txtCPF);
            this.panelCentrocadCliente.Controls.Add(this.txtNomeCliente);
            this.panelCentrocadCliente.Controls.Add(this.label5);
            this.panelCentrocadCliente.Controls.Add(this.label4);
            this.panelCentrocadCliente.Controls.Add(this.label3);
            this.panelCentrocadCliente.Controls.Add(this.label2);
            this.panelCentrocadCliente.Controls.Add(this.label1);
            this.panelCentrocadCliente.Location = new System.Drawing.Point(-1, 69);
            this.panelCentrocadCliente.Margin = new System.Windows.Forms.Padding(4);
            this.panelCentrocadCliente.Name = "panelCentrocadCliente";
            this.panelCentrocadCliente.Size = new System.Drawing.Size(1184, 569);
            this.panelCentrocadCliente.TabIndex = 25;
            // 
            // txtRamal
            // 
            this.txtRamal.Location = new System.Drawing.Point(1003, 449);
            this.txtRamal.Margin = new System.Windows.Forms.Padding(4);
            this.txtRamal.Mask = "0000";
            this.txtRamal.Name = "txtRamal";
            this.txtRamal.Size = new System.Drawing.Size(97, 22);
            this.txtRamal.TabIndex = 45;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(915, 441);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 31);
            this.label24.TabIndex = 44;
            this.label24.Text = "Ramal:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(504, 486);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(233, 22);
            this.txtEmail.TabIndex = 43;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(423, 478);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 31);
            this.label23.TabIndex = 42;
            this.label23.Text = "Email:";
            // 
            // txtTelComecial
            // 
            this.txtTelComecial.Location = new System.Drawing.Point(673, 449);
            this.txtTelComecial.Margin = new System.Windows.Forms.Padding(4);
            this.txtTelComecial.Mask = "(00)0000-0000";
            this.txtTelComecial.Name = "txtTelComecial";
            this.txtTelComecial.Size = new System.Drawing.Size(215, 22);
            this.txtTelComecial.TabIndex = 41;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label22.Location = new System.Drawing.Point(420, 441);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(257, 31);
            this.label22.TabIndex = 40;
            this.label22.Text = "Telefone Comercial:";
            // 
            // txtReferencia
            // 
            this.txtReferencia.Location = new System.Drawing.Point(567, 342);
            this.txtReferencia.Margin = new System.Windows.Forms.Padding(4);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(233, 22);
            this.txtReferencia.TabIndex = 39;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(409, 333);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(165, 31);
            this.label21.TabIndex = 38;
            this.label21.Text = "Referência:";
            // 
            // cboEstado
            // 
            this.cboEstado.FormattingEnabled = true;
            this.cboEstado.Location = new System.Drawing.Point(941, 313);
            this.cboEstado.Margin = new System.Windows.Forms.Padding(4);
            this.cboEstado.Name = "cboEstado";
            this.cboEstado.Size = new System.Drawing.Size(123, 24);
            this.cboEstado.TabIndex = 37;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label20.Location = new System.Drawing.Point(835, 304);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 31);
            this.label20.TabIndex = 36;
            this.label20.Text = "Estado:";
            // 
            // txtCep
            // 
            this.txtCep.Location = new System.Drawing.Point(567, 311);
            this.txtCep.Margin = new System.Windows.Forms.Padding(4);
            this.txtCep.Mask = "00.000.000";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(97, 22);
            this.txtCep.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(488, 303);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 31);
            this.label19.TabIndex = 34;
            this.label19.Text = "CEP:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(755, 273);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(202, 31);
            this.label18.TabIndex = 33;
            this.label18.Text = "Complemento:";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(941, 282);
            this.txtComplemento.Margin = new System.Windows.Forms.Padding(4);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(233, 22);
            this.txtComplemento.TabIndex = 32;
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(567, 282);
            this.txtNumero.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumero.Mask = "0000";
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(97, 22);
            this.txtNumero.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label17.Location = new System.Drawing.Point(449, 271);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(125, 31);
            this.label17.TabIndex = 30;
            this.label17.Text = "Número:";
            // 
            // LBLclienteDesdePF
            // 
            this.LBLclienteDesdePF.AutoSize = true;
            this.LBLclienteDesdePF.BackColor = System.Drawing.Color.Transparent;
            this.LBLclienteDesdePF.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLclienteDesdePF.Location = new System.Drawing.Point(659, 177);
            this.LBLclienteDesdePF.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLclienteDesdePF.Name = "LBLclienteDesdePF";
            this.LBLclienteDesdePF.Size = new System.Drawing.Size(128, 31);
            this.LBLclienteDesdePF.TabIndex = 29;
            this.LBLclienteDesdePF.Text = "08/11/19";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(449, 177);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(202, 31);
            this.label16.TabIndex = 28;
            this.label16.Text = "Cliente desde:";
            // 
            // dtpCliente
            // 
            this.dtpCliente.Location = new System.Drawing.Point(701, 138);
            this.dtpCliente.Margin = new System.Windows.Forms.Padding(4);
            this.dtpCliente.Name = "dtpCliente";
            this.dtpCliente.Size = new System.Drawing.Size(280, 22);
            this.dtpCliente.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(449, 127);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(287, 31);
            this.label14.TabIndex = 26;
            this.label14.Text = "Data de Nascimento:";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.Location = new System.Drawing.Point(1008, 11);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(150, 24);
            this.radioButton3.TabIndex = 25;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Pessoa Jurídica";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton4.Location = new System.Drawing.Point(859, 11);
            this.radioButton4.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(136, 24);
            this.radioButton4.TabIndex = 24;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Pessoa Física";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // txtProfissaoCliente
            // 
            this.txtProfissaoCliente.Location = new System.Drawing.Point(583, 89);
            this.txtProfissaoCliente.Margin = new System.Windows.Forms.Padding(4);
            this.txtProfissaoCliente.Name = "txtProfissaoCliente";
            this.txtProfissaoCliente.Size = new System.Drawing.Size(233, 22);
            this.txtProfissaoCliente.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(449, 80);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(146, 31);
            this.label13.TabIndex = 22;
            this.label13.Text = "Profissão:";
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(129, 486);
            this.txtCelular.Margin = new System.Windows.Forms.Padding(4);
            this.txtCelular.Mask = "(00)0000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(233, 22);
            this.txtCelular.TabIndex = 21;
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(129, 449);
            this.txtTel.Margin = new System.Windows.Forms.Padding(4);
            this.txtTel.Mask = "(00)0000-0000";
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(233, 22);
            this.txtTel.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label11.Location = new System.Drawing.Point(17, 478);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 31);
            this.label11.TabIndex = 19;
            this.label11.Text = "Celular:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label12.Location = new System.Drawing.Point(8, 441);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 31);
            this.label12.TabIndex = 18;
            this.label12.Text = "Telefone:";
            // 
            // LBLcodCliente
            // 
            this.LBLcodCliente.AutoSize = true;
            this.LBLcodCliente.BackColor = System.Drawing.Color.Transparent;
            this.LBLcodCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLcodCliente.Location = new System.Drawing.Point(120, 47);
            this.LBLcodCliente.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LBLcodCliente.Name = "LBLcodCliente";
            this.LBLcodCliente.Size = new System.Drawing.Size(30, 31);
            this.LBLcodCliente.TabIndex = 17;
            this.LBLcodCliente.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(-8, 398);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(103, 29);
            this.label10.TabIndex = 16;
            this.label10.Text = "Contato";
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(127, 311);
            this.txtBairro.Margin = new System.Windows.Forms.Padding(4);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(233, 22);
            this.txtBairro.TabIndex = 15;
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(129, 343);
            this.txtCidade.Margin = new System.Windows.Forms.Padding(4);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(233, 22);
            this.txtCidade.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(57, 271);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 31);
            this.label9.TabIndex = 13;
            this.label9.Text = "Rua:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(36, 303);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 31);
            this.label8.TabIndex = 12;
            this.label8.Text = "Bairro:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(28, 334);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 31);
            this.label7.TabIndex = 11;
            this.label7.Text = "Cidade:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(-3, 224);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 29);
            this.label6.TabIndex = 10;
            this.label6.Text = "Endereço";
            // 
            // rdnFem
            // 
            this.rdnFem.AutoSize = true;
            this.rdnFem.Location = new System.Drawing.Point(249, 187);
            this.rdnFem.Margin = new System.Windows.Forms.Padding(4);
            this.rdnFem.Name = "rdnFem";
            this.rdnFem.Size = new System.Drawing.Size(86, 21);
            this.rdnFem.TabIndex = 9;
            this.rdnFem.TabStop = true;
            this.rdnFem.Text = "Feminino";
            this.rdnFem.UseVisualStyleBackColor = true;
            // 
            // rdnMasc
            // 
            this.rdnMasc.AutoSize = true;
            this.rdnMasc.Location = new System.Drawing.Point(128, 187);
            this.rdnMasc.Margin = new System.Windows.Forms.Padding(4);
            this.rdnMasc.Name = "rdnMasc";
            this.rdnMasc.Size = new System.Drawing.Size(92, 21);
            this.rdnMasc.TabIndex = 8;
            this.rdnMasc.TabStop = true;
            this.rdnMasc.Text = "Masculino";
            this.rdnMasc.UseVisualStyleBackColor = true;
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(127, 279);
            this.txtRua.Margin = new System.Windows.Forms.Padding(4);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(233, 22);
            this.txtRua.TabIndex = 7;
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(127, 127);
            this.txtCPF.Margin = new System.Windows.Forms.Padding(4);
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(233, 22);
            this.txtCPF.TabIndex = 6;
            // 
            // txtNomeCliente
            // 
            this.txtNomeCliente.Location = new System.Drawing.Point(127, 91);
            this.txtNomeCliente.Margin = new System.Windows.Forms.Padding(4);
            this.txtNomeCliente.Name = "txtNomeCliente";
            this.txtNomeCliente.Size = new System.Drawing.Size(233, 22);
            this.txtNomeCliente.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(44, 177);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 31);
            this.label5.TabIndex = 4;
            this.label5.Text = "Sexo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(48, 118);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 31);
            this.label4.TabIndex = 3;
            this.label4.Text = "CPF:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(33, 82);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 44);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "Código:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-3, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente";
            // 
            // FRMcadastroCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1181, 699);
            this.Controls.Add(this.panelCentrocadCliente);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FRMcadastroCliente";
            this.Text = "FRMcadastroCliente";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClientePesquisar)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelCentrocadCliente.ResumeLayout(false);
            this.panelCentrocadCliente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClienteFECHAR;
        private System.Windows.Forms.Button btnClienteSALVAR;
        private System.Windows.Forms.Button btnClienteExcluir;
        private System.Windows.Forms.Button btnClienteCANCELAR;
        private System.Windows.Forms.Button btnClienteALTERAR;
        private System.Windows.Forms.Button BTNcadClienteINSERIR;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox btnClientePesquisar;
        private System.Windows.Forms.Panel panelCentrocadCliente;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dadosGeraisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem observacoesToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LBLcodCliente;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rdnFem;
        private System.Windows.Forms.RadioButton rdnMasc;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.TextBox txtCPF;
        private System.Windows.Forms.TextBox txtNomeCliente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.MaskedTextBox txtNumero;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label LBLclienteDesdePF;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpCliente;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox txtProfissaoCliente;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.MaskedTextBox txtTel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox txtRamal;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox txtTelComecial;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboEstado;
        private System.Windows.Forms.Label label20;
    }
}