﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Clientes.telas
{
    class DTOcliente
    {
        public int ID { get; set; }
        public String Nome { get; set; }

        public String CPF { get; set; }

        public String Sexo { get; set; }

        public String Profissao { get; set; }

        public DateTime Data { get; set; }

        public DateTime Inicio { get; set; }

        public String Rua { get; set; }

        public String Bairro { get; set; }

        public String CEP { get; set; }

        public String Numero { get; set; }

        public String Cidade { get; set; }

        public String Referencia { get; set; }

        public String Complemento { get; set; }

        public String Estado { get; set; }

        public String Telefone { get; set; }

        public String Telefone_Comercial { get; set; }

        public String Ramal { get; set; }

        public String Celular{ get; set; }

        public String Email { get; set; }
    }
}
