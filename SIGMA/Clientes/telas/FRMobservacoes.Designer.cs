﻿namespace SIGMA.Clientes
{
    partial class FRMobservacoes
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelObservacoesCliente = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panelObservsCliente = new System.Windows.Forms.Panel();
            this.MTBobsClientCelular2 = new System.Windows.Forms.MaskedTextBox();
            this.MTBobsClientTelefone2 = new System.Windows.Forms.MaskedTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LBLqntCaracClient = new System.Windows.Forms.Label();
            this.LBLanotClien = new System.Windows.Forms.Label();
            this.panelObservsCliente.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelObservacoesCliente
            // 
            this.panelObservacoesCliente.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panelObservacoesCliente.Location = new System.Drawing.Point(0, 3);
            this.panelObservacoesCliente.Name = "panelObservacoesCliente";
            this.panelObservacoesCliente.Size = new System.Drawing.Size(0, 0);
            this.panelObservacoesCliente.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // panelObservsCliente
            // 
            this.panelObservsCliente.BackColor = System.Drawing.Color.FloralWhite;
            this.panelObservsCliente.Controls.Add(this.MTBobsClientCelular2);
            this.panelObservsCliente.Controls.Add(this.MTBobsClientTelefone2);
            this.panelObservsCliente.Controls.Add(this.textBox1);
            this.panelObservsCliente.Controls.Add(this.label3);
            this.panelObservsCliente.Controls.Add(this.label2);
            this.panelObservsCliente.Controls.Add(this.label1);
            this.panelObservsCliente.Controls.Add(this.LBLqntCaracClient);
            this.panelObservsCliente.Controls.Add(this.LBLanotClien);
            this.panelObservsCliente.Location = new System.Drawing.Point(0, 0);
            this.panelObservsCliente.Name = "panelObservsCliente";
            this.panelObservsCliente.Size = new System.Drawing.Size(888, 462);
            this.panelObservsCliente.TabIndex = 2;
            // 
            // MTBobsClientCelular2
            // 
            this.MTBobsClientCelular2.Location = new System.Drawing.Point(108, 315);
            this.MTBobsClientCelular2.Mask = "(00)0000-0000";
            this.MTBobsClientCelular2.Name = "MTBobsClientCelular2";
            this.MTBobsClientCelular2.Size = new System.Drawing.Size(118, 20);
            this.MTBobsClientCelular2.TabIndex = 39;
            // 
            // MTBobsClientTelefone2
            // 
            this.MTBobsClientTelefone2.Location = new System.Drawing.Point(108, 285);
            this.MTBobsClientTelefone2.Mask = "(00)0000-0000";
            this.MTBobsClientTelefone2.Name = "MTBobsClientTelefone2";
            this.MTBobsClientTelefone2.Size = new System.Drawing.Size(118, 20);
            this.MTBobsClientTelefone2.TabIndex = 38;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(8, 55);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(774, 179);
            this.textBox1.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Minion Pro Cond", 16F);
            this.label3.Location = new System.Drawing.Point(7, 308);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 30);
            this.label3.TabIndex = 36;
            this.label3.Text = "Celular 2:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Minion Pro Cond", 16F);
            this.label2.Location = new System.Drawing.Point(3, 278);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 30);
            this.label2.TabIndex = 35;
            this.label2.Text = "Telefone 2:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Minion Pro Cond", 16F);
            this.label1.Location = new System.Drawing.Point(3, 248);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 30);
            this.label1.TabIndex = 34;
            this.label1.Text = "Outros Contatos";
            // 
            // LBLqntCaracClient
            // 
            this.LBLqntCaracClient.AutoSize = true;
            this.LBLqntCaracClient.Font = new System.Drawing.Font("Minion Pro Cond", 16F);
            this.LBLqntCaracClient.Location = new System.Drawing.Point(199, 22);
            this.LBLqntCaracClient.Name = "LBLqntCaracClient";
            this.LBLqntCaracClient.Size = new System.Drawing.Size(248, 30);
            this.LBLqntCaracClient.TabIndex = 33;
            this.LBLqntCaracClient.Text = "( quantidade de caracteres )";
            // 
            // LBLanotClien
            // 
            this.LBLanotClien.AutoSize = true;
            this.LBLanotClien.Font = new System.Drawing.Font("Minion Pro Cond", 16F);
            this.LBLanotClien.Location = new System.Drawing.Point(3, 22);
            this.LBLanotClien.Name = "LBLanotClien";
            this.LBLanotClien.Size = new System.Drawing.Size(205, 30);
            this.LBLanotClien.TabIndex = 32;
            this.LBLanotClien.Text = "Anotações do Cliente: ";
            // 
            // FRMobservacoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelObservsCliente);
            this.Controls.Add(this.panelObservacoesCliente);
            this.Name = "FRMobservacoes";
            this.Size = new System.Drawing.Size(888, 462);
            this.panelObservsCliente.ResumeLayout(false);
            this.panelObservsCliente.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelObservacoesCliente;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.Panel panelObservsCliente;
        private System.Windows.Forms.MaskedTextBox MTBobsClientCelular2;
        private System.Windows.Forms.MaskedTextBox MTBobsClientTelefone2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LBLqntCaracClient;
        private System.Windows.Forms.Label LBLanotClien;
    }
}
