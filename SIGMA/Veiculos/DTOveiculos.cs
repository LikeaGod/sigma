﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Veiculos
{
    class DTOveiculos
    {
        public int ID_Veiculos { get; set; }
        public string Placa { get; set; }
        public string Modelo { get; set; }
        public string Proprietario { get; set; }
        public string Cor { get; set; }
        public int Odometro { get; set; }
        public string Combustivel { get; set; }
        public string Observacao { get; set; }
        public string Ano_Fabric { get; set; }
    }
}
