﻿namespace SIGMA.Veiculos
{
    partial class frmLocalizarModelo
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLocalizarVeiculos = new System.Windows.Forms.Panel();
            this.btnCANCELARlocalizarVeiculo = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dgvLocalizar = new System.Windows.Forms.DataGridView();
            this.rdnCodigo = new System.Windows.Forms.RadioButton();
            this.rdnDescricao = new System.Windows.Forms.RadioButton();
            this.txtTipodeBusca = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Código = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.de = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.até = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelLocalizarVeiculos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalizar)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLocalizarVeiculos
            // 
            this.panelLocalizarVeiculos.BackColor = System.Drawing.Color.SeaShell;
            this.panelLocalizarVeiculos.Controls.Add(this.btnCANCELARlocalizarVeiculo);
            this.panelLocalizarVeiculos.Controls.Add(this.btnOK);
            this.panelLocalizarVeiculos.Controls.Add(this.dgvLocalizar);
            this.panelLocalizarVeiculos.Controls.Add(this.rdnCodigo);
            this.panelLocalizarVeiculos.Controls.Add(this.rdnDescricao);
            this.panelLocalizarVeiculos.Controls.Add(this.txtTipodeBusca);
            this.panelLocalizarVeiculos.Controls.Add(this.label2);
            this.panelLocalizarVeiculos.Controls.Add(this.label1);
            this.panelLocalizarVeiculos.Location = new System.Drawing.Point(0, 0);
            this.panelLocalizarVeiculos.Margin = new System.Windows.Forms.Padding(4);
            this.panelLocalizarVeiculos.Name = "panelLocalizarVeiculos";
            this.panelLocalizarVeiculos.Size = new System.Drawing.Size(1153, 512);
            this.panelLocalizarVeiculos.TabIndex = 0;
            // 
            // btnCANCELARlocalizarVeiculo
            // 
            this.btnCANCELARlocalizarVeiculo.Location = new System.Drawing.Point(1029, 441);
            this.btnCANCELARlocalizarVeiculo.Margin = new System.Windows.Forms.Padding(4);
            this.btnCANCELARlocalizarVeiculo.Name = "btnCANCELARlocalizarVeiculo";
            this.btnCANCELARlocalizarVeiculo.Size = new System.Drawing.Size(100, 41);
            this.btnCANCELARlocalizarVeiculo.TabIndex = 7;
            this.btnCANCELARlocalizarVeiculo.Text = "CANCELAR";
            this.btnCANCELARlocalizarVeiculo.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(893, 441);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 41);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dgvLocalizar
            // 
            this.dgvLocalizar.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvLocalizar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocalizar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Código,
            this.Modelo,
            this.de,
            this.até});
            this.dgvLocalizar.Location = new System.Drawing.Point(11, 134);
            this.dgvLocalizar.Margin = new System.Windows.Forms.Padding(4);
            this.dgvLocalizar.Name = "dgvLocalizar";
            this.dgvLocalizar.ReadOnly = true;
            this.dgvLocalizar.RowHeadersVisible = false;
            this.dgvLocalizar.Size = new System.Drawing.Size(1119, 279);
            this.dgvLocalizar.TabIndex = 5;
            // 
            // rdnCodigo
            // 
            this.rdnCodigo.AutoSize = true;
            this.rdnCodigo.Location = new System.Drawing.Point(11, 48);
            this.rdnCodigo.Margin = new System.Windows.Forms.Padding(4);
            this.rdnCodigo.Name = "rdnCodigo";
            this.rdnCodigo.Size = new System.Drawing.Size(73, 21);
            this.rdnCodigo.TabIndex = 4;
            this.rdnCodigo.TabStop = true;
            this.rdnCodigo.Text = "Código";
            this.rdnCodigo.UseVisualStyleBackColor = true;
            // 
            // rdnDescricao
            // 
            this.rdnDescricao.AutoSize = true;
            this.rdnDescricao.Location = new System.Drawing.Point(11, 76);
            this.rdnDescricao.Margin = new System.Windows.Forms.Padding(4);
            this.rdnDescricao.Name = "rdnDescricao";
            this.rdnDescricao.Size = new System.Drawing.Size(92, 21);
            this.rdnDescricao.TabIndex = 3;
            this.rdnDescricao.TabStop = true;
            this.rdnDescricao.Text = "Descrição";
            this.rdnDescricao.UseVisualStyleBackColor = true;
            // 
            // txtTipodeBusca
            // 
            this.txtTipodeBusca.Location = new System.Drawing.Point(324, 44);
            this.txtTipodeBusca.Margin = new System.Windows.Forms.Padding(4);
            this.txtTipodeBusca.Name = "txtTipodeBusca";
            this.txtTipodeBusca.Size = new System.Drawing.Size(571, 22);
            this.txtTipodeBusca.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(317, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(689, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Escolha o tipo de Busca, digite o valor e aperte ENTER";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label1.Location = new System.Drawing.Point(4, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de Busca";
            // 
            // Código
            // 
            this.Código.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Código.DataPropertyName = "ID_Veiculos";
            this.Código.HeaderText = "Código";
            this.Código.Name = "Código";
            this.Código.ReadOnly = true;
            // 
            // Modelo
            // 
            this.Modelo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Modelo.DataPropertyName = "Modelo";
            this.Modelo.HeaderText = "modelo de veiculo";
            this.Modelo.Name = "Modelo";
            this.Modelo.ReadOnly = true;
            // 
            // de
            // 
            this.de.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.de.DataPropertyName = "Ano_Fabric";
            this.de.HeaderText = "de";
            this.de.Name = "de";
            this.de.ReadOnly = true;
            // 
            // até
            // 
            this.até.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.até.DataPropertyName = "Ano_Fabric";
            this.até.HeaderText = "até";
            this.até.Name = "até";
            this.até.ReadOnly = true;
            // 
            // frmLocalizarModelo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelLocalizarVeiculos);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmLocalizarModelo";
            this.Size = new System.Drawing.Size(1153, 512);
            this.panelLocalizarVeiculos.ResumeLayout(false);
            this.panelLocalizarVeiculos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocalizar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLocalizarVeiculos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTipodeBusca;
        private System.Windows.Forms.RadioButton rdnCodigo;
        private System.Windows.Forms.RadioButton rdnDescricao;
        private System.Windows.Forms.Button btnCANCELARlocalizarVeiculo;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DataGridView dgvLocalizar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Código;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn de;
        private System.Windows.Forms.DataGridViewTextBoxColumn até;
    }
}
