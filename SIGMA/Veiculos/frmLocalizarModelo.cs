﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA.Veiculos
{
    public partial class frmLocalizarModelo : UserControl
    {
        public frmLocalizarModelo()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            BusinessVeiculos veic = new BusinessVeiculos();
            BindingList<DTOveiculos> dtolista = new BindingList<DTOveiculos>();
            DTOveiculos dto = new DTOveiculos();
            if (rdnCodigo.Checked == true)
            {
                dto.ID_Veiculos = Convert.ToInt32(txtTipodeBusca.Text.Trim());
                dto = veic.Consultar_ID(dto);
            }
            else if (rdnDescricao.Checked == true)
            {
                dto.Modelo = txtTipodeBusca.Text.Trim();
                dto = veic.Consultar_Descricao(dto);
            }
            else
            {
                MessageBox.Show("Selecione um tipo");
            }
            dgvLocalizar.AutoGenerateColumns = false;
            dtolista.Add(dto);
            dgvLocalizar.DataSource = dtolista;
               
        }
    }
}
