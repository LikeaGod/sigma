﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIGMA.Veiculos
{
    public partial class frmCadastrodeVeiculos : Form
    {
        public frmCadastrodeVeiculos()
        {
            InitializeComponent();
            Carregar_Combos();
        }
        public void Carregar_Combos()
        {
            List<string> Anos = new List<string>();
            List<string> Combust = new List<string>();
            Anos.Add("1980");
            Anos.Add("1981");
            Anos.Add("1982");
            Anos.Add("1983");
            Anos.Add("1984");
            Anos.Add("1985");
            Anos.Add("1986");
            Anos.Add("1987");
            Anos.Add("1988");
            Anos.Add("1989");
            Anos.Add("1990");
            Anos.Add("1991");
            Anos.Add("1992");
            Anos.Add("1993");
            Anos.Add("1994");
            Anos.Add("1995");
            Anos.Add("1996");
            Anos.Add("1997");
            Anos.Add("1998");
            Anos.Add("1999");
            Anos.Add("2000");
            Anos.Add("2001");
            Anos.Add("2002");
            Anos.Add("2003");
            Anos.Add("2004");
            Anos.Add("2005");
            Anos.Add("2006");
            Anos.Add("2007");
            Anos.Add("2008");
            Anos.Add("2009");
            Anos.Add("2010");
            Anos.Add("2011");
            Anos.Add("2012");
            Anos.Add("2013");
            Anos.Add("2014");
            Anos.Add("2015");
            Anos.Add("2016");
            Anos.Add("2017");
            Anos.Add("2018");

            Combust.Add("Gasolina");
            Combust.Add("Etanol");
            Combust.Add("Diesel");
            Combust.Add("GNV");

            cboAnodeFabricação.DataSource = Anos;
            cboCombustivel.DataSource = Combust;
        }

        private void btnSALVAR_Click(object sender, EventArgs e)
        {
            DTOveiculos dto = new DTOveiculos();
            dto.Ano_Fabric = cboAnodeFabricação.SelectedItem.ToString();
            dto.Proprietario = txtProprietario.Text;
            dto.Placa = txtPLACA.Text;
            dto.Odometro = Convert.ToInt32(nudOdometro.Value);
            dto.Observacao = txtObs.Text;
            dto.Modelo = txtModelo.Text;
            dto.Cor = txtCor.Text;
            dto.Combustivel = cboCombustivel.SelectedItem.ToString();

            BusinessVeiculos buss = new BusinessVeiculos();
            int id = buss.Salvar(dto);


        }
    }
}
