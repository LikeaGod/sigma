﻿namespace SIGMA.Veiculos
{
    partial class frmCadastrodeVeiculos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelCima = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelBotoes = new System.Windows.Forms.Panel();
            this.btnFECHAR = new System.Windows.Forms.Button();
            this.btnSALVAR = new System.Windows.Forms.Button();
            this.btnCANCELAR = new System.Windows.Forms.Button();
            this.btnALTERAR = new System.Windows.Forms.Button();
            this.bntEXCLUIR = new System.Windows.Forms.Button();
            this.btnINSERIR = new System.Windows.Forms.Button();
            this.panelCENTRO = new System.Windows.Forms.Panel();
            this.nudOdometro = new System.Windows.Forms.NumericUpDown();
            this.txtModelo = new System.Windows.Forms.TextBox();
            this.txtObs = new System.Windows.Forms.TextBox();
            this.cboCombustivel = new System.Windows.Forms.ComboBox();
            this.cboAnodeFabricação = new System.Windows.Forms.ComboBox();
            this.txtCor = new System.Windows.Forms.TextBox();
            this.txtPLACA = new System.Windows.Forms.TextBox();
            this.txtProprietario = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblNumeroCodigo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelCima.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelBotoes.SuspendLayout();
            this.panelCENTRO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOdometro)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCima
            // 
            this.panelCima.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panelCima.Controls.Add(this.pictureBox1);
            this.panelCima.Location = new System.Drawing.Point(0, 0);
            this.panelCima.Margin = new System.Windows.Forms.Padding(4);
            this.panelCima.Name = "panelCima";
            this.panelCima.Size = new System.Drawing.Size(1153, 59);
            this.panelCima.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.pictureBox1.Image = global::SIGMA.Properties.Resources._13d99f3d0cf4aec174b5869efa816e13__cone_da_lupa_by_vexels;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(68, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelBotoes
            // 
            this.panelBotoes.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panelBotoes.Controls.Add(this.btnFECHAR);
            this.panelBotoes.Controls.Add(this.btnSALVAR);
            this.panelBotoes.Controls.Add(this.btnCANCELAR);
            this.panelBotoes.Controls.Add(this.btnALTERAR);
            this.panelBotoes.Controls.Add(this.bntEXCLUIR);
            this.panelBotoes.Controls.Add(this.btnINSERIR);
            this.panelBotoes.Location = new System.Drawing.Point(0, 560);
            this.panelBotoes.Margin = new System.Windows.Forms.Padding(4);
            this.panelBotoes.Name = "panelBotoes";
            this.panelBotoes.Size = new System.Drawing.Size(1153, 69);
            this.panelBotoes.TabIndex = 0;
            // 
            // btnFECHAR
            // 
            this.btnFECHAR.Location = new System.Drawing.Point(1037, 16);
            this.btnFECHAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnFECHAR.Name = "btnFECHAR";
            this.btnFECHAR.Size = new System.Drawing.Size(100, 37);
            this.btnFECHAR.TabIndex = 5;
            this.btnFECHAR.Text = "FECHAR";
            this.btnFECHAR.UseVisualStyleBackColor = true;
            // 
            // btnSALVAR
            // 
            this.btnSALVAR.Location = new System.Drawing.Point(929, 16);
            this.btnSALVAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnSALVAR.Name = "btnSALVAR";
            this.btnSALVAR.Size = new System.Drawing.Size(100, 37);
            this.btnSALVAR.TabIndex = 4;
            this.btnSALVAR.Text = "SALVAR";
            this.btnSALVAR.UseVisualStyleBackColor = true;
            this.btnSALVAR.Click += new System.EventHandler(this.btnSALVAR_Click);
            // 
            // btnCANCELAR
            // 
            this.btnCANCELAR.Location = new System.Drawing.Point(240, 16);
            this.btnCANCELAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnCANCELAR.Name = "btnCANCELAR";
            this.btnCANCELAR.Size = new System.Drawing.Size(100, 37);
            this.btnCANCELAR.TabIndex = 3;
            this.btnCANCELAR.Text = "CANCELAR";
            this.btnCANCELAR.UseVisualStyleBackColor = true;
            // 
            // btnALTERAR
            // 
            this.btnALTERAR.Location = new System.Drawing.Point(120, 16);
            this.btnALTERAR.Margin = new System.Windows.Forms.Padding(4);
            this.btnALTERAR.Name = "btnALTERAR";
            this.btnALTERAR.Size = new System.Drawing.Size(100, 37);
            this.btnALTERAR.TabIndex = 2;
            this.btnALTERAR.Text = "ALTERAR";
            this.btnALTERAR.UseVisualStyleBackColor = true;
            // 
            // bntEXCLUIR
            // 
            this.bntEXCLUIR.Location = new System.Drawing.Point(361, 16);
            this.bntEXCLUIR.Margin = new System.Windows.Forms.Padding(4);
            this.bntEXCLUIR.Name = "bntEXCLUIR";
            this.bntEXCLUIR.Size = new System.Drawing.Size(100, 37);
            this.bntEXCLUIR.TabIndex = 1;
            this.bntEXCLUIR.Text = "EXCLUIR";
            this.bntEXCLUIR.UseVisualStyleBackColor = true;
            // 
            // btnINSERIR
            // 
            this.btnINSERIR.Location = new System.Drawing.Point(4, 16);
            this.btnINSERIR.Margin = new System.Windows.Forms.Padding(4);
            this.btnINSERIR.Name = "btnINSERIR";
            this.btnINSERIR.Size = new System.Drawing.Size(100, 37);
            this.btnINSERIR.TabIndex = 0;
            this.btnINSERIR.Text = "INSERIR";
            this.btnINSERIR.UseVisualStyleBackColor = true;
            // 
            // panelCENTRO
            // 
            this.panelCENTRO.BackgroundImage = global::SIGMA.Properties.Resources.x;
            this.panelCENTRO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelCENTRO.Controls.Add(this.nudOdometro);
            this.panelCENTRO.Controls.Add(this.txtModelo);
            this.panelCENTRO.Controls.Add(this.txtObs);
            this.panelCENTRO.Controls.Add(this.cboCombustivel);
            this.panelCENTRO.Controls.Add(this.cboAnodeFabricação);
            this.panelCENTRO.Controls.Add(this.txtCor);
            this.panelCENTRO.Controls.Add(this.txtPLACA);
            this.panelCENTRO.Controls.Add(this.txtProprietario);
            this.panelCENTRO.Controls.Add(this.label10);
            this.panelCENTRO.Controls.Add(this.label9);
            this.panelCENTRO.Controls.Add(this.label8);
            this.panelCENTRO.Controls.Add(this.label7);
            this.panelCENTRO.Controls.Add(this.lblNumeroCodigo);
            this.panelCENTRO.Controls.Add(this.label5);
            this.panelCENTRO.Controls.Add(this.label4);
            this.panelCENTRO.Controls.Add(this.label3);
            this.panelCENTRO.Controls.Add(this.label2);
            this.panelCENTRO.Controls.Add(this.label1);
            this.panelCENTRO.Location = new System.Drawing.Point(0, 57);
            this.panelCENTRO.Margin = new System.Windows.Forms.Padding(4);
            this.panelCENTRO.Name = "panelCENTRO";
            this.panelCENTRO.Size = new System.Drawing.Size(1153, 512);
            this.panelCENTRO.TabIndex = 0;
            // 
            // nudOdometro
            // 
            this.nudOdometro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nudOdometro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.nudOdometro.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.nudOdometro.InterceptArrowKeys = false;
            this.nudOdometro.Location = new System.Drawing.Point(801, 118);
            this.nudOdometro.Name = "nudOdometro";
            this.nudOdometro.Size = new System.Drawing.Size(160, 22);
            this.nudOdometro.TabIndex = 18;
            this.nudOdometro.ThousandsSeparator = true;
            // 
            // txtModelo
            // 
            this.txtModelo.Location = new System.Drawing.Point(223, 145);
            this.txtModelo.Margin = new System.Windows.Forms.Padding(4);
            this.txtModelo.Name = "txtModelo";
            this.txtModelo.Size = new System.Drawing.Size(185, 22);
            this.txtModelo.TabIndex = 17;
            // 
            // txtObs
            // 
            this.txtObs.Location = new System.Drawing.Point(4, 302);
            this.txtObs.Margin = new System.Windows.Forms.Padding(4);
            this.txtObs.Multiline = true;
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(1132, 194);
            this.txtObs.TabIndex = 16;
            // 
            // cboCombustivel
            // 
            this.cboCombustivel.FormattingEnabled = true;
            this.cboCombustivel.Location = new System.Drawing.Point(801, 66);
            this.cboCombustivel.Margin = new System.Windows.Forms.Padding(4);
            this.cboCombustivel.Name = "cboCombustivel";
            this.cboCombustivel.Size = new System.Drawing.Size(160, 24);
            this.cboCombustivel.TabIndex = 15;
            // 
            // cboAnodeFabricação
            // 
            this.cboAnodeFabricação.FormattingEnabled = true;
            this.cboAnodeFabricação.Location = new System.Drawing.Point(801, 206);
            this.cboAnodeFabricação.Margin = new System.Windows.Forms.Padding(4);
            this.cboAnodeFabricação.Name = "cboAnodeFabricação";
            this.cboAnodeFabricação.Size = new System.Drawing.Size(160, 24);
            this.cboAnodeFabricação.TabIndex = 14;
            // 
            // txtCor
            // 
            this.txtCor.Location = new System.Drawing.Point(801, 168);
            this.txtCor.Margin = new System.Windows.Forms.Padding(4);
            this.txtCor.Name = "txtCor";
            this.txtCor.Size = new System.Drawing.Size(160, 22);
            this.txtCor.TabIndex = 12;
            // 
            // txtPLACA
            // 
            this.txtPLACA.Location = new System.Drawing.Point(223, 61);
            this.txtPLACA.Margin = new System.Windows.Forms.Padding(4);
            this.txtPLACA.Name = "txtPLACA";
            this.txtPLACA.Size = new System.Drawing.Size(185, 22);
            this.txtPLACA.TabIndex = 11;
            // 
            // txtProprietario
            // 
            this.txtProprietario.Location = new System.Drawing.Point(223, 101);
            this.txtProprietario.Margin = new System.Windows.Forms.Padding(4);
            this.txtProprietario.Name = "txtProprietario";
            this.txtProprietario.Size = new System.Drawing.Size(185, 22);
            this.txtProprietario.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(100, 138);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 29);
            this.label10.TabIndex = 9;
            this.label10.Text = "Modelo:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(47, 94);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(163, 29);
            this.label9.TabIndex = 8;
            this.label9.Text = "Proprietário:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(120, 51);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 29);
            this.label8.TabIndex = 7;
            this.label8.Text = "Placa:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(635, 111);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 29);
            this.label7.TabIndex = 6;
            this.label7.Text = "Odômetro:";
            // 
            // lblNumeroCodigo
            // 
            this.lblNumeroCodigo.AutoSize = true;
            this.lblNumeroCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.lblNumeroCodigo.Location = new System.Drawing.Point(218, 4);
            this.lblNumeroCodigo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumeroCodigo.Name = "lblNumeroCodigo";
            this.lblNumeroCodigo.Size = new System.Drawing.Size(28, 29);
            this.lblNumeroCodigo.TabIndex = 5;
            this.lblNumeroCodigo.Text = "1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(712, 161);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 29);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cor:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(534, 201);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(242, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ano de fabricação:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(9, 265);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "Observações:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(603, 61);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Combustivel:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(101, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Código:";
            // 
            // frmCadastrodeVeiculos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1153, 628);
            this.Controls.Add(this.panelBotoes);
            this.Controls.Add(this.panelCENTRO);
            this.Controls.Add(this.panelCima);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmCadastrodeVeiculos";
            this.Text = "frmCadastrodeVeiculos";
            this.panelCima.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelBotoes.ResumeLayout(false);
            this.panelCENTRO.ResumeLayout(false);
            this.panelCENTRO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOdometro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCima;
        private System.Windows.Forms.Panel panelBotoes;
        private System.Windows.Forms.Panel panelCENTRO;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblNumeroCodigo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFECHAR;
        private System.Windows.Forms.Button btnSALVAR;
        private System.Windows.Forms.Button btnCANCELAR;
        private System.Windows.Forms.Button btnALTERAR;
        private System.Windows.Forms.Button bntEXCLUIR;
        private System.Windows.Forms.Button btnINSERIR;
        private System.Windows.Forms.TextBox txtModelo;
        private System.Windows.Forms.TextBox txtObs;
        private System.Windows.Forms.ComboBox cboCombustivel;
        private System.Windows.Forms.ComboBox cboAnodeFabricação;
        private System.Windows.Forms.TextBox txtCor;
        private System.Windows.Forms.TextBox txtPLACA;
        private System.Windows.Forms.TextBox txtProprietario;
        private System.Windows.Forms.NumericUpDown nudOdometro;
    }
}