﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Veiculos
{
    class BusinessVeiculos
    {
        public int Salvar(DTOveiculos dto)
        { 
        DatabaseVeiculos dbveic = new DatabaseVeiculos();
        int id = dbveic.Salvar(dto);
        return id;
        }
        public DTOveiculos Consultar_ID(DTOveiculos dto)
        {
            DatabaseVeiculos dbveic = new DatabaseVeiculos();
            dto = dbveic.Consultar_ID(dto);
            return dto;
        }
        public DTOveiculos Consultar_Descricao(DTOveiculos dto)
        {
            DatabaseVeiculos dbveic = new DatabaseVeiculos();
            dto = dbveic.Consultar_Descricao(dto);
            return dto;
        }
    }
}
