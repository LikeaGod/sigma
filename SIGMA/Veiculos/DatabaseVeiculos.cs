﻿using MySql.Data.MySqlClient;
using SIGMA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Veiculos
{
    class DatabaseVeiculos
    {
        public int Salvar(DTOveiculos dto)
        {
            string script = @"INSERT INTO tb_veiculos(ds_placa, nm_proprietario, ds_cor, ds_modelo, ds_odometro, ds_combustivel, ds_observacao, dt_ano_fabric) VALUES (@ds_placa, @nm_proprietario, @ds_cor, @ds_modelo, @ds_odometro, @ds_combustivel, @ds_observacao, @dt_ano_fabric)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_placa" , dto.Placa));
            parms.Add(new MySqlParameter("nm_proprietario", dto.Proprietario));
            parms.Add(new MySqlParameter("ds_cor" , dto.Cor));
            parms.Add(new MySqlParameter("ds_modelo" , dto.Modelo));
            parms.Add(new MySqlParameter("ds_odometro" , dto.Odometro));
            parms.Add(new MySqlParameter("ds_combustivel" , dto.Combustivel));
            parms.Add(new MySqlParameter("ds_observacao" , dto.Observacao));
            parms.Add(new MySqlParameter("dt_ano_fabric" , dto.Ano_Fabric));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }
        public DTOveiculos Consultar_ID(DTOveiculos dto)
        {
            string script = @"select * from tb_veiculos where id_veiculos = @id_veiculos";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_veiculos", dto.ID_Veiculos));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            while (reader.Read())
            {
                dto.Modelo = reader.GetString("ds_modelo");
                dto.Ano_Fabric = reader.GetString("dt_ano_fabric");
                dto.Combustivel = reader.GetString("ds_combustivel");
                dto.Cor = reader.GetString("ds_cor");
                dto.Observacao = reader.GetString("ds_observacao");
                dto.Odometro = reader.GetInt32("ds_odometro");
                dto.Placa = reader.GetString("ds_placa");
                dto.Proprietario = reader.GetString("nm_proprietario");
            }
            return dto;
        }
        public DTOveiculos Consultar_Descricao(DTOveiculos dto)
        {
            string script = @"select * from tb_veiculos where ds_modelo = @ds_modelo";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", dto.Modelo));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            while (reader.Read())
            {
                dto.Modelo = reader.GetString("ds_modelo");
                dto.Ano_Fabric = reader.GetString("dt_ano_fabric");
                dto.Combustivel = reader.GetString("ds_combustivel");
                dto.Cor = reader.GetString("ds_cor");
                dto.Observacao = reader.GetString("ds_observacao");
                dto.Odometro = reader.GetInt32("ds_odometro");
                dto.Placa = reader.GetString("ds_placa");
                dto.Proprietario = reader.GetString("nm_proprietario");
            }

            return dto;
        }
    }
}
