﻿using SIGMA.UsuariodoSistema.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA
{
    static class SessaoUsuarioLogado
    {
        public static UsuarioDTO Logado { get; set; }
    }
}
