﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIGMA.Funcionarios
{
    class DTOFuncionarios
    {
        public int Id_Funcionario { get; set; }
        public string Nome { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public DateTime Nascimento { get; set; }
        public Enum Sexo { get; set; }
        public string Numero  { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string Referencia { get; set; }
        public string Complemento { get; set; }
        public string Estado { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Celular{ get; set; }
        

    }
}
