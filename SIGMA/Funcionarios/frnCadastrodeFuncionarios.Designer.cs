﻿namespace SIGMA.Funcionarios
{
    partial class frnCadastrodeFuncionarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelCima = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelBaxo = new System.Windows.Forms.Panel();
            this.btnFECHAR = new System.Windows.Forms.Button();
            this.btnSALVAR = new System.Windows.Forms.Button();
            this.btnEXCLUIR = new System.Windows.Forms.Button();
            this.btnCANCELAR = new System.Windows.Forms.Button();
            this.btnINSERIR = new System.Windows.Forms.Button();
            this.panelCentro = new System.Windows.Forms.Panel();
            this.mtbTelefone = new System.Windows.Forms.MaskedTextBox();
            this.mtbCelular = new System.Windows.Forms.MaskedTextBox();
            this.txtEMAIL = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lbl21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.mtbCEP = new System.Windows.Forms.MaskedTextBox();
            this.cboESTADO = new System.Windows.Forms.ComboBox();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.txtReferencia = new System.Windows.Forms.TextBox();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rdnMasculino = new System.Windows.Forms.RadioButton();
            this.rdnFeminino = new System.Windows.Forms.RadioButton();
            this.btnBuscarFoto = new System.Windows.Forms.PictureBox();
            this.fotoFuncionario = new System.Windows.Forms.PictureBox();
            this.cboDepartamento = new System.Windows.Forms.ComboBox();
            this.dateDatadeNascimento = new System.Windows.Forms.DateTimePicker();
            this.mtbCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtRG = new System.Windows.Forms.TextBox();
            this.txtFOTO = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNumeroCodigo = new System.Windows.Forms.Label();
            this.panelCima.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelBaxo.SuspendLayout();
            this.panelCentro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscarFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFuncionario)).BeginInit();
            this.SuspendLayout();
            // 
            // panelCima
            // 
            this.panelCima.BackColor = System.Drawing.Color.Black;
            this.panelCima.Controls.Add(this.pictureBox1);
            this.panelCima.Location = new System.Drawing.Point(0, 0);
            this.panelCima.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelCima.Name = "panelCima";
            this.panelCima.Size = new System.Drawing.Size(1375, 60);
            this.panelCima.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImage = global::SIGMA.Properties.Resources.usuario_masculino_simbolo_interface_de_pesquisa_318_39648;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(4, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 60);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelBaxo
            // 
            this.panelBaxo.BackColor = System.Drawing.Color.Black;
            this.panelBaxo.Controls.Add(this.btnFECHAR);
            this.panelBaxo.Controls.Add(this.btnSALVAR);
            this.panelBaxo.Controls.Add(this.btnEXCLUIR);
            this.panelBaxo.Controls.Add(this.btnCANCELAR);
            this.panelBaxo.Controls.Add(this.btnINSERIR);
            this.panelBaxo.Location = new System.Drawing.Point(0, 722);
            this.panelBaxo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelBaxo.Name = "panelBaxo";
            this.panelBaxo.Size = new System.Drawing.Size(1375, 68);
            this.panelBaxo.TabIndex = 0;
            // 
            // btnFECHAR
            // 
            this.btnFECHAR.Location = new System.Drawing.Point(1257, 15);
            this.btnFECHAR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFECHAR.Name = "btnFECHAR";
            this.btnFECHAR.Size = new System.Drawing.Size(100, 39);
            this.btnFECHAR.TabIndex = 1;
            this.btnFECHAR.Text = "FECHAR";
            this.btnFECHAR.UseVisualStyleBackColor = true;
            // 
            // btnSALVAR
            // 
            this.btnSALVAR.Location = new System.Drawing.Point(1135, 15);
            this.btnSALVAR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSALVAR.Name = "btnSALVAR";
            this.btnSALVAR.Size = new System.Drawing.Size(100, 39);
            this.btnSALVAR.TabIndex = 2;
            this.btnSALVAR.Text = "SALVAR";
            this.btnSALVAR.UseVisualStyleBackColor = true;
            // 
            // btnEXCLUIR
            // 
            this.btnEXCLUIR.Location = new System.Drawing.Point(269, 15);
            this.btnEXCLUIR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEXCLUIR.Name = "btnEXCLUIR";
            this.btnEXCLUIR.Size = new System.Drawing.Size(115, 39);
            this.btnEXCLUIR.TabIndex = 3;
            this.btnEXCLUIR.Text = "EXCLUIR";
            this.btnEXCLUIR.UseVisualStyleBackColor = true;
            // 
            // btnCANCELAR
            // 
            this.btnCANCELAR.Location = new System.Drawing.Point(147, 15);
            this.btnCANCELAR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCANCELAR.Name = "btnCANCELAR";
            this.btnCANCELAR.Size = new System.Drawing.Size(115, 39);
            this.btnCANCELAR.TabIndex = 4;
            this.btnCANCELAR.Text = "CANCELAR";
            this.btnCANCELAR.UseVisualStyleBackColor = true;
            // 
            // btnINSERIR
            // 
            this.btnINSERIR.Location = new System.Drawing.Point(16, 15);
            this.btnINSERIR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnINSERIR.Name = "btnINSERIR";
            this.btnINSERIR.Size = new System.Drawing.Size(116, 39);
            this.btnINSERIR.TabIndex = 5;
            this.btnINSERIR.Text = "INSERIR";
            this.btnINSERIR.UseVisualStyleBackColor = true;
            // 
            // panelCentro
            // 
            this.panelCentro.BackgroundImage = global::SIGMA.Properties.Resources.pessoa_de_negocios_macho_segurando_uma_mala_318_63795;
            this.panelCentro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelCentro.Controls.Add(this.mtbTelefone);
            this.panelCentro.Controls.Add(this.mtbCelular);
            this.panelCentro.Controls.Add(this.txtEMAIL);
            this.panelCentro.Controls.Add(this.label22);
            this.panelCentro.Controls.Add(this.lbl21);
            this.panelCentro.Controls.Add(this.label20);
            this.panelCentro.Controls.Add(this.label19);
            this.panelCentro.Controls.Add(this.mtbCEP);
            this.panelCentro.Controls.Add(this.cboESTADO);
            this.panelCentro.Controls.Add(this.txtRua);
            this.panelCentro.Controls.Add(this.txtBairro);
            this.panelCentro.Controls.Add(this.txtCidade);
            this.panelCentro.Controls.Add(this.txtNumero);
            this.panelCentro.Controls.Add(this.txtReferencia);
            this.panelCentro.Controls.Add(this.txtComplemento);
            this.panelCentro.Controls.Add(this.label18);
            this.panelCentro.Controls.Add(this.label17);
            this.panelCentro.Controls.Add(this.label16);
            this.panelCentro.Controls.Add(this.label15);
            this.panelCentro.Controls.Add(this.label14);
            this.panelCentro.Controls.Add(this.label13);
            this.panelCentro.Controls.Add(this.label12);
            this.panelCentro.Controls.Add(this.label11);
            this.panelCentro.Controls.Add(this.label1);
            this.panelCentro.Controls.Add(this.rdnMasculino);
            this.panelCentro.Controls.Add(this.rdnFeminino);
            this.panelCentro.Controls.Add(this.btnBuscarFoto);
            this.panelCentro.Controls.Add(this.fotoFuncionario);
            this.panelCentro.Controls.Add(this.cboDepartamento);
            this.panelCentro.Controls.Add(this.dateDatadeNascimento);
            this.panelCentro.Controls.Add(this.mtbCPF);
            this.panelCentro.Controls.Add(this.txtNome);
            this.panelCentro.Controls.Add(this.txtRG);
            this.panelCentro.Controls.Add(this.txtFOTO);
            this.panelCentro.Controls.Add(this.label10);
            this.panelCentro.Controls.Add(this.label9);
            this.panelCentro.Controls.Add(this.label8);
            this.panelCentro.Controls.Add(this.label7);
            this.panelCentro.Controls.Add(this.label6);
            this.panelCentro.Controls.Add(this.label5);
            this.panelCentro.Controls.Add(this.label4);
            this.panelCentro.Controls.Add(this.label3);
            this.panelCentro.Controls.Add(this.label2);
            this.panelCentro.Controls.Add(this.lblNumeroCodigo);
            this.panelCentro.Location = new System.Drawing.Point(0, 55);
            this.panelCentro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelCentro.Name = "panelCentro";
            this.panelCentro.Size = new System.Drawing.Size(1375, 674);
            this.panelCentro.TabIndex = 0;
            // 
            // mtbTelefone
            // 
            this.mtbTelefone.Location = new System.Drawing.Point(145, 581);
            this.mtbTelefone.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbTelefone.Mask = "(00) 0000-0000";
            this.mtbTelefone.Name = "mtbTelefone";
            this.mtbTelefone.Size = new System.Drawing.Size(212, 22);
            this.mtbTelefone.TabIndex = 87;
            // 
            // mtbCelular
            // 
            this.mtbCelular.Location = new System.Drawing.Point(145, 611);
            this.mtbCelular.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbCelular.Mask = "(00) 0000-0000";
            this.mtbCelular.Name = "mtbCelular";
            this.mtbCelular.Size = new System.Drawing.Size(212, 22);
            this.mtbCelular.TabIndex = 86;
            // 
            // txtEMAIL
            // 
            this.txtEMAIL.Location = new System.Drawing.Point(145, 551);
            this.txtEMAIL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEMAIL.Name = "txtEMAIL";
            this.txtEMAIL.Size = new System.Drawing.Size(212, 22);
            this.txtEMAIL.TabIndex = 85;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label22.Location = new System.Drawing.Point(9, 476);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 29);
            this.label22.TabIndex = 84;
            this.label22.Text = "Contato";
            // 
            // lbl21
            // 
            this.lbl21.AutoSize = true;
            this.lbl21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lbl21.Location = new System.Drawing.Point(51, 544);
            this.lbl21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl21.Name = "lbl21";
            this.lbl21.Size = new System.Drawing.Size(86, 29);
            this.lbl21.TabIndex = 83;
            this.lbl21.Text = "Email:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(12, 574);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(125, 29);
            this.label20.TabIndex = 82;
            this.label20.Text = "Telefone:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(33, 604);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(104, 29);
            this.label19.TabIndex = 81;
            this.label19.Text = "Celular:";
            // 
            // mtbCEP
            // 
            this.mtbCEP.Location = new System.Drawing.Point(671, 369);
            this.mtbCEP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbCEP.Mask = "00.000.000";
            this.mtbCEP.Name = "mtbCEP";
            this.mtbCEP.Size = new System.Drawing.Size(152, 22);
            this.mtbCEP.TabIndex = 80;
            // 
            // cboESTADO
            // 
            this.cboESTADO.FormattingEnabled = true;
            this.cboESTADO.Location = new System.Drawing.Point(1177, 361);
            this.cboESTADO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboESTADO.Name = "cboESTADO";
            this.cboESTADO.Size = new System.Drawing.Size(180, 24);
            this.cboESTADO.TabIndex = 79;
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(195, 320);
            this.txtRua.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(272, 22);
            this.txtRua.TabIndex = 78;
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(195, 367);
            this.txtBairro.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(272, 22);
            this.txtBairro.TabIndex = 77;
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(195, 412);
            this.txtCidade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(272, 22);
            this.txtCidade.TabIndex = 76;
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(671, 322);
            this.txtNumero.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(152, 22);
            this.txtNumero.TabIndex = 75;
            // 
            // txtReferencia
            // 
            this.txtReferencia.Location = new System.Drawing.Point(671, 412);
            this.txtReferencia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtReferencia.Name = "txtReferencia";
            this.txtReferencia.Size = new System.Drawing.Size(152, 22);
            this.txtReferencia.TabIndex = 74;
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(1177, 320);
            this.txtComplemento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(179, 22);
            this.txtComplemento.TabIndex = 73;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label18.Location = new System.Drawing.Point(124, 315);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(66, 29);
            this.label18.TabIndex = 72;
            this.label18.Text = "Rua:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label17.Location = new System.Drawing.Point(7, 282);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(126, 29);
            this.label17.TabIndex = 71;
            this.label17.Text = "Endereço";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label16.Location = new System.Drawing.Point(99, 362);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 29);
            this.label16.TabIndex = 70;
            this.label16.Text = "Bairro:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(516, 405);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(147, 29);
            this.label15.TabIndex = 69;
            this.label15.Text = "Referência:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(986, 315);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(183, 29);
            this.label14.TabIndex = 68;
            this.label14.Text = "Complemento:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(591, 362);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 29);
            this.label13.TabIndex = 67;
            this.label13.Text = "CEP:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(86, 405);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 29);
            this.label12.TabIndex = 66;
            this.label12.Text = "Cidade:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(550, 315);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 29);
            this.label11.TabIndex = 65;
            this.label11.Text = "Número:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(1068, 354);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 29);
            this.label1.TabIndex = 64;
            this.label1.Text = "Estado:";
            // 
            // rdnMasculino
            // 
            this.rdnMasculino.AutoSize = true;
            this.rdnMasculino.Location = new System.Drawing.Point(216, 165);
            this.rdnMasculino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdnMasculino.Name = "rdnMasculino";
            this.rdnMasculino.Size = new System.Drawing.Size(92, 21);
            this.rdnMasculino.TabIndex = 63;
            this.rdnMasculino.TabStop = true;
            this.rdnMasculino.Text = "Masculino";
            this.rdnMasculino.UseVisualStyleBackColor = true;
            // 
            // rdnFeminino
            // 
            this.rdnFeminino.AutoSize = true;
            this.rdnFeminino.Location = new System.Drawing.Point(344, 165);
            this.rdnFeminino.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rdnFeminino.Name = "rdnFeminino";
            this.rdnFeminino.Size = new System.Drawing.Size(86, 21);
            this.rdnFeminino.TabIndex = 62;
            this.rdnFeminino.TabStop = true;
            this.rdnFeminino.Text = "Feminino";
            this.rdnFeminino.UseVisualStyleBackColor = true;
            // 
            // btnBuscarFoto
            // 
            this.btnBuscarFoto.BackColor = System.Drawing.Color.SeaShell;
            this.btnBuscarFoto.BackgroundImage = global::SIGMA.Properties.Resources._13d99f3d0cf4aec174b5869efa816e13__cone_da_lupa_by_vexels;
            this.btnBuscarFoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscarFoto.Location = new System.Drawing.Point(426, 227);
            this.btnBuscarFoto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBuscarFoto.Name = "btnBuscarFoto";
            this.btnBuscarFoto.Size = new System.Drawing.Size(41, 32);
            this.btnBuscarFoto.TabIndex = 61;
            this.btnBuscarFoto.TabStop = false;
            // 
            // fotoFuncionario
            // 
            this.fotoFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fotoFuncionario.Image = global::SIGMA.Properties.Resources.person_5_icon_icons_com_68898;
            this.fotoFuncionario.Location = new System.Drawing.Point(1089, 46);
            this.fotoFuncionario.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.fotoFuncionario.Name = "fotoFuncionario";
            this.fotoFuncionario.Size = new System.Drawing.Size(252, 176);
            this.fotoFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.fotoFuncionario.TabIndex = 60;
            this.fotoFuncionario.TabStop = false;
            // 
            // cboDepartamento
            // 
            this.cboDepartamento.FormattingEnabled = true;
            this.cboDepartamento.Location = new System.Drawing.Point(921, 46);
            this.cboDepartamento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboDepartamento.Name = "cboDepartamento";
            this.cboDepartamento.Size = new System.Drawing.Size(160, 24);
            this.cboDepartamento.TabIndex = 59;
            // 
            // dateDatadeNascimento
            // 
            this.dateDatadeNascimento.Location = new System.Drawing.Point(813, 164);
            this.dateDatadeNascimento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateDatadeNascimento.Name = "dateDatadeNascimento";
            this.dateDatadeNascimento.Size = new System.Drawing.Size(281, 22);
            this.dateDatadeNascimento.TabIndex = 58;
            // 
            // mtbCPF
            // 
            this.mtbCPF.Location = new System.Drawing.Point(616, 201);
            this.mtbCPF.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbCPF.Mask = "000.000.000-00";
            this.mtbCPF.Name = "mtbCPF";
            this.mtbCPF.Size = new System.Drawing.Size(221, 22);
            this.mtbCPF.TabIndex = 57;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(195, 100);
            this.txtNome.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(272, 22);
            this.txtNome.TabIndex = 56;
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(195, 132);
            this.txtRG.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(272, 22);
            this.txtRG.TabIndex = 55;
            // 
            // txtFOTO
            // 
            this.txtFOTO.Location = new System.Drawing.Point(195, 197);
            this.txtFOTO.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFOTO.Name = "txtFOTO";
            this.txtFOTO.Size = new System.Drawing.Size(272, 22);
            this.txtFOTO.TabIndex = 54;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label10.Location = new System.Drawing.Point(4, 30);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(164, 29);
            this.label10.TabIndex = 53;
            this.label10.Text = "Funcionários";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(537, 160);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(255, 29);
            this.label9.TabIndex = 52;
            this.label9.Text = "Data de Nascimento:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(124, 191);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 29);
            this.label8.TabIndex = 51;
            this.label8.Text = "Foto:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(110, 93);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 29);
            this.label7.TabIndex = 50;
            this.label7.Text = "Nome:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(537, 193);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 29);
            this.label6.TabIndex = 49;
            this.label6.Text = "CPF:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(140, 126);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 29);
            this.label5.TabIndex = 48;
            this.label5.Text = "RG:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(701, 46);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(212, 29);
            this.label4.TabIndex = 47;
            this.label4.Text = "Departamanento:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(118, 160);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 29);
            this.label3.TabIndex = 46;
            this.label3.Text = "Sexo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(93, 63);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 29);
            this.label2.TabIndex = 45;
            this.label2.Text = "Código:";
            // 
            // lblNumeroCodigo
            // 
            this.lblNumeroCodigo.AutoSize = true;
            this.lblNumeroCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
            this.lblNumeroCodigo.Location = new System.Drawing.Point(202, 63);
            this.lblNumeroCodigo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumeroCodigo.Name = "lblNumeroCodigo";
            this.lblNumeroCodigo.Size = new System.Drawing.Size(41, 29);
            this.lblNumeroCodigo.TabIndex = 44;
            this.lblNumeroCodigo.Text = "01";
            // 
            // frnCadastrodeFuncionarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1376, 791);
            this.Controls.Add(this.panelCentro);
            this.Controls.Add(this.panelBaxo);
            this.Controls.Add(this.panelCima);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frnCadastrodeFuncionarios";
            this.Text = "frnCadastrodeFuncionarios";
            this.panelCima.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelBaxo.ResumeLayout(false);
            this.panelCentro.ResumeLayout(false);
            this.panelCentro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscarFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fotoFuncionario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCima;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelCentro;
        private System.Windows.Forms.MaskedTextBox mtbTelefone;
        private System.Windows.Forms.MaskedTextBox mtbCelular;
        private System.Windows.Forms.TextBox txtEMAIL;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lbl21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox mtbCEP;
        private System.Windows.Forms.ComboBox cboESTADO;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.TextBox txtReferencia;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdnMasculino;
        private System.Windows.Forms.RadioButton rdnFeminino;
        private System.Windows.Forms.PictureBox btnBuscarFoto;
        private System.Windows.Forms.PictureBox fotoFuncionario;
        private System.Windows.Forms.ComboBox cboDepartamento;
        private System.Windows.Forms.DateTimePicker dateDatadeNascimento;
        private System.Windows.Forms.MaskedTextBox mtbCPF;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtRG;
        private System.Windows.Forms.TextBox txtFOTO;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNumeroCodigo;
        private System.Windows.Forms.Panel panelBaxo;
        private System.Windows.Forms.Button btnFECHAR;
        private System.Windows.Forms.Button btnSALVAR;
        private System.Windows.Forms.Button btnEXCLUIR;
        private System.Windows.Forms.Button btnCANCELAR;
        private System.Windows.Forms.Button btnINSERIR;
    }
}